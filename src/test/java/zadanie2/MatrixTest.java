package zadanie2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class MatrixTest {

    private Matrix matrixA;
    private Matrix matrixB;

    @BeforeEach
    void setMatrix() {
        matrixA = new Matrix(new double[][]{{1, 2, 3},
                                    {3, 2, 1},
                                    {2, 1, 3}});
        matrixB = new Matrix(new double[][]{{2, 3, 5},
                                    {4, 5, 1},
                                    {6, 9, 3}});
        /*  {{1,2,3},{3,2,1},{2,1,3}}  {{2,3,5},{4,5,1},{6,9,3}}*/
    }

    @Nested
    @DisplayName("testing setters and getters")
    class set_get{

        @Test
        @DisplayName("get_cell(int row,int column)")
        void get1(){
            assertEquals(1, matrixA.get_cell(0,0));
            assertEquals(3, matrixA.get_cell(0,2));
            assertEquals(5,matrixB.get_cell(1,1));
            assertEquals(9,matrixB.get_cell(2,1));
        }

        @Test
        @DisplayName("set_cell(int row , int column,double value)")
        void set1(){
            matrixA.set_cell(0,0,100);
            assertEquals(100,matrixA.get_cell(0,0));

            matrixA.set_cell(2,2,-100);
            assertEquals(-100,matrixA.get_cell(2,2));

        }

        @Test
        @DisplayName("Exception on get method")
        void getException(){
            assertThrows(IllegalArgumentException.class,()->{
                matrixA.get_cell(3,3);
            });
        }

        @Test
        @DisplayName("Exception on set method")
        void setException(){
            assertThrows(IllegalArgumentException.class,()->{
                matrixA.set_cell(3,3,33);
            });

        }
    }

    @Nested
    @DisplayName("add method testing")
    class adding{
        @Test
        @DisplayName("return sum of two matrix")
        void add1(){
            Matrix AplusB = new Matrix(new double[][]{{3, 5, 8}, {7, 7, 2}, {8, 10, 6}});
            assertEquals(matrixA.add(matrixB),AplusB);
        }

        @Test
        @DisplayName("throw exception when bad size of matrix")
        void addExceptionThrow(){
            Matrix matrix = new Matrix(5);
            assertThrows(IllegalArgumentException.class,()->{
               matrix.add(matrixA);
            });
        }
    }

    @Nested
    @DisplayName("Other")
    class Other{

        @Test
        @DisplayName("equels method")
        void equelstTest(){
            Matrix matrixAprime = new Matrix(new double[][]{{1, 2, 3},
                    {3, 2, 1},
                    {2, 1, 3}});
            assertTrue(matrixA.equals(matrixA));
            assertTrue(matrixA.equals(matrixAprime));
        }

        @Test
        @DisplayName("return minor of matrix")
        void minorTest(){
            /*  {{1,2,3},{3,2,1},{2,1,3}}  {{2,3,5},{4,5,1},{6,9,3}}*/
            /* {{1,3},{2,3}}*/
            Matrix matrix = new Matrix(new double[][]{{1, 3}, {2, 3}});
            assertEquals(matrix,matrixA.minor(1,1));

            Matrix matrix1 = new Matrix(new double[][]{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}});
            Matrix matrix1MinorR3C3 = new Matrix(new double[][]{{1, 2, 3}, {5, 6, 7}, {9, 10, 11}});
            assertEquals(matrix1MinorR3C3,matrix1.minor(3,3));

            Matrix matrix2 = new Matrix(new double[][]{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}});
            Matrix matrixR0C0 = new Matrix(new double[][]{{6, 7, 8}, {10, 11, 12}, {14, 15, 16}});
            assertEquals(matrixR0C0,matrix2.minor(0,0));
        }
    }

    @Nested
    @DisplayName("Multiplay")
    class multiplyMethodTest{

        @Test
        @DisplayName("multiplay A*B should return NEW matrix")
        void multiplyTest(){
            /*
            {{1,2,3},{3,2,1},{2,1,3}} * {{2,3,5},{4,5,1},{6,9,3}}
            ={{28, 40, 16}, {20, 28, 20}, {26, 38, 20}}
            */
            Matrix result = new Matrix(new double[][]{{28, 40, 16}, {20, 28, 20}, {26, 38, 20}});
            assertEquals(result,matrixA.multiply(matrixB));
        }

        @Test
        @DisplayName("multiplay B*A should return NEW matrix")
        void multiplyTest2(){
            /*
             {{2,3,5},{4,5,1},{6,9,3}}*{1,2,3},{3,2,1},{2,1,3}}
            {{21, 15, 24}, {21, 19, 20}, {39, 33, 36}}
            */
            Matrix result = new Matrix(new double[][]{{21, 15, 24}, {21, 19, 20}, {39, 33, 36}});
            assertEquals(result,matrixB.multiply(matrixA));
        }

        @Test
        @DisplayName("multiplay coefficient*A should return NEW matrix")
        void multiplyTest3(){
            /*
               2 * {{1,2,3},{3,2,1},{2,1,3}} =   {{2, 4, 6}, {6, 4, 2}, {4, 2, 6}}
            */
            Matrix result = new Matrix(new double[][]{{2, 4, 6}, {6, 4, 2}, {4, 2, 6}});
            assertEquals(result,matrixA.multiply(2));
        }
    }

    @Nested
    @DisplayName("Det method")
    class detTest{
        @Test
        @DisplayName("Det matrix 1x1")
        void detTest1(){
            Matrix matrix1x1 = new Matrix(new double[][]{{1}});
            assertEquals(1,matrix1x1.det());
        }

        @Test
        @DisplayName("Det matrix 2x2")
        void detTest2(){
            Matrix matrix = new Matrix(new double[][]{{3, 9}, {12, 18}});
            assertEquals(-54,matrix.det());

            Matrix matrix1 = new Matrix(new double[][]{{6, 3}, {76, 2}});
            assertEquals(-216,matrix1.det());
        }

        @Test
        @DisplayName("det matrix 3x3")
        void detTest3(){
            Matrix matrix = new Matrix(new double[][]{{5, 3, 8}, {2, 4, 3}, {82, 73, 2}});
            assertEquals(-1785,matrix.det());

            Matrix matrix1 = new Matrix(new double[][]{{23,53,23},{34,2,65},{34,23,8}});
            assertEquals(85119,matrix1.det());

        }

        @Test
        @DisplayName("det matrix 4x4")
        void detTest4(){
            Matrix matrix = new Matrix(new double[][]{{1, 6, 2, 4}, {2, 6, 1, 9}, {3, 8, 5, 2}, {2, 6, 3, 6}});
            assertEquals(-68,matrix.det());

            Matrix matrix1 = new Matrix(new double[][]{{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}});
            assertEquals(0,matrix1.det());
        }

        @Test
        @DisplayName("det matrix 5x5")
        void detTest5(){
            Matrix matrix = new Matrix(new double[][]{{1, 1, 1, 1, 1}, {1, 1, 1, 1, 1}, {1, 1, 1, 1, 1}, {1, 1, 1, 1, 1}, {1, 1, 1, 1, 1}});
            assertEquals(0,matrix.det());
        }

    }
}