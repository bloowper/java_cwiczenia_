package zadanie4;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NodeTest{
    @Test
    @DisplayName("Equels test")
    void basicEquelsTest(){
        Node<String, Integer> tekst1 = new Node<>("tekst1", 1);
        Node<String, Integer> tekst11 = new Node<>("tekst1", 12);
        assertTrue(tekst1.equals(tekst11));
        Node<String, Integer> stringIntegerNode = new Node<>("tekst1-edit", 1);
        assertFalse(tekst1.equals(stringIntegerNode));
    }


}