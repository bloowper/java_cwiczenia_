package zadanie4;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MapTest{

    zadanie4.Map<Integer,String> map;
    @BeforeEach
    void set(){
        map = new Map<>();
    }

    @Test
    void zlapraktykatest(){
        assertEquals(0,map.size(),"new map have to be empty");
        map.put(10,"test");
        assertEquals(1,map.size(),"put element should expand size");
        assertEquals("test",map.get(10));
        map.put(10,"test2");
        assertEquals("test2",map.get(10));
        map.put(10,"nowawartoscpod10");
        assertEquals("nowawartoscpod10",map.get(10));
        assertTrue(map.containsKey(10));
        assertFalse(map.containsKey(11));
    }

    @Test
    @DisplayName("Visitor test")
    void visitorTest(){
        map.put(1,"jeden");
        map.put(2,"dwa");
        map.put(3,"trzy");
        map.put(4,"cztery");
        map.put(5,"piec");
        map.put(6,"szesc");
        map.acceptVisitor(new Visitor<Integer>() {
            @Override
            public boolean shouldRemove(Integer key) { return key%2==0; }
        });
        assertNotNull(map.get(1));
        assertNull(map.get(2));
        assertNotNull(map.get(3));
        assertNull(map.get(4));
        assertNotNull(map.get(5));
        assertNull(map.get(6));
    }

}