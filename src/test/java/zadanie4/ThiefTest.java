package zadanie4;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ThiefTest{
    @Test
    void thieftest1(){
        Thief thief = new Thief(1000);
        assertTrue(thief.shouldRemove(1001));
        assertFalse(thief.shouldRemove(1000));
    }
}