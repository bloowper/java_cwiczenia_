package zadanie7;

import org.junit.jupiter.api.Test;
import zadanie6.Status;
import zadanie7.exceptions.*;
import static org.junit.jupiter.api.Assertions.*;

class TicTacToeMultiplayerTest {

    @Test
    void setCell() throws Exception {
        TicTacToeMultiplayer ttt = new TicTacToeMultiplayer(5, 5, Status.O);
        ttt.SETCURRENTSHAPE(Status.O);
        try {
            ttt.setCell(0,0,Status.O);
        } catch (zadanie6.notAllowed notAllowed) {
            fail();
        }
        assertThrows(notYourTurnException.class,() -> {ttt.setCell(2,2,Status.O);});
        ttt.setPlayerShape(Status.X);
        ttt.SETCURRENTSHAPE(Status.X);
        ttt.restartGame();
        ttt.setCell(0,0,Status.X);
        assertThrows(notYourTurnException.class,() -> {ttt.setCell(2,2,Status.O);});

    }
}