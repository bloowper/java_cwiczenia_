package zadanie6;

import org.apache.maven.surefire.shared.lang3.tuple.MutablePair;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

class TicTacToeUtilitiesTest {

    @Test
    void returnListOfDiagonals() {
        // Test wizualny xD
        // nie chcialem tracic zbednego czasu
        // a wydawalo mi sie ze tutaj poszlo by go sporo
        ArrayList<ArrayList<MutablePair<Integer, Integer>>> diagonals1 = TicTacToeUtilities.returnListOfDiagonals(3);
        for (ArrayList<MutablePair<Integer, Integer>> diag : diagonals1) {
            System.out.println(diag);
        }

        ArrayList<ArrayList<MutablePair<Integer, Integer>>> diagonals2 = TicTacToeUtilities.returnListOfDiagonals(5);
        for (ArrayList<MutablePair<Integer, Integer>> diag : diagonals2) {
            System.out.println(diag);
        }
    }
}