package zadanie6;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class TicTacToeTest {

    TicTacToe ticTacToe3x3;
    TicTacToe ticTacToe4x4;
    TicTacToe ticTacToe5x5;
    @BeforeEach
    void setUp() {
        ticTacToe3x3 = new TicTacToe(3);
        ticTacToe4x4 = new TicTacToe(4);
        ticTacToe5x5 = new TicTacToe(5);
    }

    @Test
    @DisplayName("Array list 2d N x N Status.CLEAR")
    @Order(0)
    void constructor(){
        for(ArrayList<Cell> row : ticTacToe3x3.field){
            for(Cell c:row){
                assertEquals(c.status,Status.CLEAR);
            }
        }
        for(ArrayList<Cell> row : ticTacToe4x4.field){
            for(Cell c:row){
                assertEquals(c.status,Status.CLEAR);
            }
        }
        for(ArrayList<Cell> row : ticTacToe5x5.field){
            for(Cell c:row){
                assertEquals(c.status,Status.CLEAR);
            }
        }

    }


    @Test
    @DisplayName("setting element in game field to specific enum Status")
    @Order(1)//potrzebuje tej metody do dalszych testow :)
    void setCell() {
        try {
            for(int w=0;w<ticTacToe3x3.field.size();w++){
                for(int k=0;k<ticTacToe3x3.field.size();k++){
                    final Status o = Status.O;
                    ticTacToe3x3.setCell(w,k,o);
                    assertEquals(o,ticTacToe3x3.field.get(w).get(k).status);
                    int finalW = w;
                    int finalK = k;
                    assertThrows(notAllowed.class,() -> ticTacToe3x3.setCell(finalW, finalK,o));
                }
            }
        } catch (notAllowed e) { e.printStackTrace(); }
        ticTacToe3x3.restartGame();


    }

    @Test
    @DisplayName("Gdy gracz wygrywa zwraca jego reprezentanta w postaci enum Status")
    void checkWon() throws notAllowed {
        assertNull(ticTacToe3x3.checkWon());
        for(int i=0;i<this.ticTacToe3x3.size;i++){ticTacToe3x3.setCell(0,i,Status.X);}
        assertEquals(Status.X,ticTacToe3x3.checkWon());
        assertThrows(notAllowed.class,() -> ticTacToe3x3.setCell(0,0,Status.O));

        assertNull(this.ticTacToe4x4.checkWon());
        for(int i=0;i<ticTacToe4x4.size;i++) {
            ticTacToe4x4.setCell(i,i,Status.O);
        }
        assertEquals(Status.O,ticTacToe4x4.checkWon());
        assertThrows(notAllowed.class,() -> ticTacToe4x4.setCell(ticTacToe4x4.size-1,ticTacToe4x4.size-1,Status.X));


        assertNull(ticTacToe5x5.checkWon());
        ticTacToe5x5.setCell(0,4,Status.O);
        ticTacToe5x5.setCell(1,3,Status.O);
        ticTacToe5x5.setCell(2,2,Status.O);
        ticTacToe5x5.setCell(3,1,Status.O);
        ticTacToe5x5.setCell(4,0,Status.O);
        assertEquals(Status.O,ticTacToe5x5.checkWon());

        ticTacToe3x3.restartGame();
        ticTacToe3x3.setCell(0,0,Status.X);
        ticTacToe3x3.setCell(1,0, Status.X);
        ticTacToe3x3.setCell(2,0,Status.X);
        assertEquals(Status.X,ticTacToe3x3.checkWon());
        ticTacToe3x3.restartGame();
        assertNull(ticTacToe3x3.checkWon());

        ticTacToe5x5.restartGame();
        ticTacToe5x5.setWon_size(3);
        ticTacToe5x5.setCell(2,1,Status.O);
        ticTacToe5x5.setCell(3,2,Status.O);
        ticTacToe5x5.setCell(4,3,Status.O);
        assertEquals(Status.O,ticTacToe5x5.checkWon());

    }

    @Test
    @DisplayName("wygrana dla dowolnie ustawionej ilsoci pol")
    void chechWonAnyQuantiti(){
        try {
            //Sprawdzenie dla wierszy kolumn
            ticTacToe3x3.setWon_size(2);
            ticTacToe3x3.setCell(0,0,Status.X);
            ticTacToe3x3.setCell(0,1,Status.X);
            assertEquals(Status.X,ticTacToe3x3.checkWon());
            ticTacToe3x3.restartGame();

            ticTacToe3x3.setCell(0,0,Status.X);
            ticTacToe3x3.setCell(1,0,Status.X);
            assertEquals(Status.X,ticTacToe3x3.checkWon());

            ticTacToe5x5.setWon_size(3);
            ticTacToe5x5.setCell(1,1,Status.O);
            ticTacToe5x5.setCell(2,1,Status.O);
            ticTacToe5x5.setCell(3,1,Status.O);
            assertEquals(Status.O,ticTacToe5x5.checkWon());
            //sprawdzenie dla ukosow

            ticTacToe5x5.restartGame();
            ticTacToe5x5.setWon_size(3);
            ticTacToe5x5.setCell(1,1,Status.O);
            ticTacToe5x5.setCell(2,2,Status.O);
            ticTacToe5x5.setCell(3,3,Status.O);
            assertEquals(Status.O,ticTacToe5x5.checkWon());



        } catch (notAllowed e) {
            e.printStackTrace();
        }
    }


}