package zadanie3;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import static org.junit.jupiter.api.Assertions.*;

class PriorityQueueTest {

    @Test
    @DisplayName("1)simple test of add get")
    void simpleTest(){
        PriorityQueue<String> queue = new PriorityQueue<String>();
        String str1="1";
        String str2="2";
        String str3="3";
        String str4="4";
        String str5="5";
        queue.add(str5,5);
        queue.add(str4,5);
        queue.add(str3,5);
        queue.add(str2,2);
        queue.add(str1,1);
        //str5 str4 str3 str2 str1
        assertEquals(str5,queue.get());
        assertEquals(str4,queue.get());
        assertEquals(str3,queue.get());
        assertEquals(str2,queue.get());
        assertEquals(str1,queue.get());
        assertNull(queue.get());

        queue.add(str5,1);
        queue.add(str4,2);
        queue.add(str3,3);
        queue.add(str2,4);
        queue.add(str1,4);
        assertEquals(str2,queue.get());
        assertEquals(str1,queue.get());
        assertEquals(str3,queue.get());
        assertEquals(str4,queue.get());
        assertEquals(str5,queue.get());
        assertNull(queue.get());
        //skonczyc

    }
}