package zadanie5;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class MyUtilitiesTest {

    @Test
    @DisplayName("should return number of non white characters")
    void countNonWhite(){
        String onlyWhite1 = "\n\n\t   \n\t\n";
        String noneWhite1 = "WHITEwhite";
        String sum1 = onlyWhite1 +noneWhite1;
        assertEquals(noneWhite1.length(),MyUtilities.numberNonWhite(sum1));

        String onlyWhite2 = "                 ";
        String noneWhite2 = "fajsdifewurwosidhkgdigrtdlghsdfkghit";
        String sum2 = onlyWhite2 + noneWhite2 + onlyWhite2;
        assertEquals(noneWhite2.length(),MyUtilities.numberNonWhite(sum2));
    }

    @Test
    @DisplayName("should return number of words in string")
    void numberOfWords(){
        String str = "jeden dwa trzy cztery piec szesc siedem, osiem dziewiec. dziesiec";
        int nOfWords = 10;
        assertEquals(nOfWords,MyUtilities.numberOfWords(str));
    }

    @Test
    @DisplayName("map with K:word V:n of word occurrences ")
    void wordHistogram(){
        String str = "jeden dwa dwa trzy trzy trzy CZTERY ,cztery.cztery cztery\n" +
                "piec, piec piec piec PiEc/";
        HashMap<String, Integer> shouldReturn = new HashMap<>() {{
            put("jeden", 1);
            put("dwa", 2);
            put("trzy", 3);
            put("cztery", 4);
            put("piec", 5);
        }};
        HashMap<String, Integer> returned = MyUtilities.wordHistogram(str);
        assertTrue(shouldReturn.equals(returned));
    }

    @Test
    @DisplayName("should return number of sentences in string")
    void numberOfSentences(){
        String str = "pierwsze zdanie jest tuta." +
                "drugie zdanie jest tutaj!" +
                "trzecie jest tutaj.." +
                "czwarte jest tutaj.";
        int nof = 4;
        assertEquals(4,MyUtilities.numberOfSentences(str));
    }
}