package zadanie5;

import javafx.util.Pair;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;


public class Main {

    /**
     * return information about text file passed as start parrameter
     * @param args file name to analise
     *
     */
    public static void main(String[] args){
        String data = "";
        try(BufferedReader br = new BufferedReader(new FileReader(args[0]))){
            char[] chars = new char[125];
            final StringBuilder stringBuilder = new StringBuilder();
            while(br.read(chars)!=-1)
                stringBuilder.append(chars);
            data = stringBuilder.toString();
        }catch (IOException exception){
            exception.printStackTrace();
        }
        int nOfNotWhiteSpace = MyUtilities.numberNonWhite(data);
        int nOfWord = MyUtilities.numberOfWords(data);
        int nOfSentences = MyUtilities.numberOfSentences(data);
        HashMap<String, Integer> wordHistogram = MyUtilities.wordHistogram(data);

        //wordHistogram processing
        ArrayList<Pair<String, Integer>> pairs = new ArrayList<Pair<String, Integer>>();
        wordHistogram.forEach((k,v)->{pairs.add(new Pair<>(k,v));});
        Collections.sort(pairs,(o1, o2) -> {return o2.getValue()-o1.getValue();});
        if(pairs.size()>10){
            int lovestAllowed = pairs.get(9).getValue();
            pairs.removeIf(si -> si.getValue()<lovestAllowed);
        }

        System.out.printf("liczba bialych %d\n",nOfNotWhiteSpace);
        System.out.printf("liczba wyrazow %d\n",nOfWord);
        System.out.printf("Liczba zdan %d\n",nOfSentences);
        System.out.println("histogram:\n"+pairs);

    }
}
