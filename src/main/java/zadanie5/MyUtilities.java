package zadanie5;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.regex.*;

/**
 * Utilities for zadanie5
 * @author Tomasz Orchowski
 * @version 0.1
 *
 */
public class MyUtilities {
    /**
     * count number of non white chars
     * @param str String to analise
     * @return int number of non white chars
     */
     public static int numberNonWhite(@NotNull String str){
        int nOfWhite = 0;
         Pattern pattern = Pattern.compile("[\\S]");
         Matcher matcher = pattern.matcher(str);
         while (matcher.find())nOfWhite++;
         return nOfWhite;
    }

    /** count number of words in string passed to function
     * @param str string to analise
     * @return int number of words in param passed
     */
    public static int numberOfWords(@NotNull String str){
        //zakladam ze slowo moze skladac sie z alfanumerykow
        int nofw =0;
        Pattern pattern = Pattern.compile("[a-zA-Z0-9]+");
        Matcher matcher = pattern.matcher(str);
        while(matcher.find())nofw++;
        return nofw;
    }


    /** "A sentence is a series of characters, starting
     *  with at lease one whitespace character,
     *  that ends in one of ., ! or ?"
     * @param str string to analise
     * @return int number of senteces in str param
     */
    public static int numberOfSentences(@NotNull String str){
        //regex z stackoverflow
        Matcher matcher = Pattern.compile("\\s+[^.!?]*[.!?]").matcher(str);
        int nof=0;
        while(matcher.find())nof++;
        return nof;
    }

    /**
     * @param str string to analise
     * @return return HashMap<String,Integer> K:word V:occurrences in str
     */
    public static HashMap<String,Integer> wordHistogram(@NotNull String str){
        HashMap<String, Integer> map = new HashMap<>();
        Matcher matcher = Pattern.compile("[a-zA-Z0-9]+").matcher(str);
        while(matcher.find()){
            String substring = str.substring(matcher.start(), matcher.end()).toLowerCase();
            if(map.containsKey(substring)) map.put(substring,map.get(substring)+1);
            else map.put(substring,1);
        }
        return map;
    }
}
