package zadanie7;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class ConnectionSimple {
    Socket socket;
    BufferedWriter writer;
    BufferedReader reader;
    boolean serwer;

    public ConnectionSimple(String ip,int port) {
        try {
            socket = new Socket(ip, port);
            writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            serwer = false;
        } catch (IOException e) {
            try {
                ServerSocket serverSocket = new ServerSocket(port);
                System.out.println("Rozpoczecie oczekiwania na klienta...");
                socket = serverSocket.accept();//w tym miejscu CHCE zeby program sie zatrzymal
                System.out.println("klient sie podlaczyl");
                writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                serwer = true;
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }

    public boolean isSerwer() {
        return serwer;
    }
}
