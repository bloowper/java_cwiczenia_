package zadanie7;

import org.apache.maven.surefire.shared.lang3.tuple.MutablePair;
import zadanie6.Status;
import zadanie6.TicTacToeUtilities;
import zadanie6.notAllowed;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;



//Debugowanie programu GUI to jest jakies zlo ;---------------------------;
public class GUI extends JFrame implements Runnable,ActionListener{

    TicTacToeMultiplayer ticTacToeMultiplayer;
    ArrayList<ArrayList<JButton>> buttonFieldArray2dButton;
    ConnectionSimple connectionSimple;
    int numOfOccupiedFields;
    int size;
    int winSize;
    public GUI(int size, int winsize) throws HeadlessException {
        super();
        this.size = size;
        numOfOccupiedFields=0;
        ticTacToeMultiplayer = new TicTacToeMultiplayer(size,winsize);
    }

    @Override
    public void run() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        JPanel jPanelMain = new JPanel();
        JPanel panelButtonField = new JPanel();
        panelButtonField.setLayout(new GridLayout(size,size));
        buttonFieldArray2dButton = createButtons();
        for(ArrayList<JButton> row : buttonFieldArray2dButton){
            for(JButton button:row){
                panelButtonField.add(button);
            }
        }
        jPanelMain.add(panelButtonField);

        this.getContentPane().add(jPanelMain);
        this.pack();
        this.setVisible(true);
        //SEKCJA ODBIERANIA OD PRZECIWNIKA
        Thread enemyThread = new Thread(() -> {
            try {
                String s;
                while(true){
                    System.out.println("Watek do odbierania! dziala ");
                    if((s = connectionSimple.reader.readLine())!=null){
                        System.out.println("cos odebrano ");
                        String[] strings = s.split(" ");
                        MutablePair<Integer, Integer> opponentChoice = new MutablePair<>(Integer.valueOf(strings[0]), Integer.valueOf(strings[1]));
                        this.ticTacToeMultiplayer.setCell(opponentChoice.left,opponentChoice.right, TicTacToeUtilities.oppositeShape(ticTacToeMultiplayer.yourShape));
                        buttonFieldArray2dButton.get(opponentChoice.left).get(opponentChoice.right).setText(this.ticTacToeMultiplayer.oppositeShape.toString());
                        System.out.println("Przeciwnik "+opponentChoice.left+" "+opponentChoice.right);
                        final Status checkWon = ticTacToeMultiplayer.checkWon();
                        if(checkWon!=null){
                            numOfOccupiedFields=0;
                            System.out.printf("%s WON!",checkWon.toString());
                            restartGame();
                        }
                        if(numOfOccupiedFields == size*size){
                            System.out.println("RESTART");
                            restartGame();
                        }
                    }
                    else {
                        System.out.println("nic nie odeebrano");
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (zadanie6.notAllowed notAllowed) { }
        });
        enemyThread.start();

        //KONIEC SEKCJI ODIBERANIA OD PRZECIWNIKA
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //wiersz \n kolumna
        final String actionCommand = e.getActionCommand();
        final String[] split = actionCommand.split("\n");
        var wiersz = Integer.valueOf(split[0]);
        var kolumna = Integer.valueOf(split[1]);
        System.out.printf("wcisnieto %d %d\n",wiersz,kolumna);
        try {
            final Status pleyerMove = ticTacToeMultiplayer.next_pleyer_move;
            ticTacToeMultiplayer.setCell(wiersz,kolumna, ticTacToeMultiplayer.yourShape);
            buttonFieldArray2dButton.get(wiersz).get(kolumna).setText(pleyerMove.toString());
            numOfOccupiedFields++;
            this.connectionSimple.writer.write(wiersz+" "+kolumna+"\n");
            this.connectionSimple.writer.flush();
        } catch (notAllowed allowed) {
            System.out.println("To pole jest obecnie nie dostepne");
        }catch (IOException notAllowedField){
            System.out.println("problem z wyslaniem");
        }

        final Status checkWon = ticTacToeMultiplayer.checkWon();
        if(checkWon!=null){
            numOfOccupiedFields=0;
            System.out.printf("%s WON!",checkWon.toString());
            restartGame();
        }
        if(numOfOccupiedFields == size*size){
            System.out.println("RESTART");
            restartGame();
        }
    }

    private ArrayList<ArrayList<JButton>> createButtons(){
        var toplvl = new ArrayList<ArrayList<JButton>>();
        for(int w=0;w<size;w++){
            var row = new ArrayList<JButton>();
            for(int k=0;k<size;k++){
                String acctionCommand = Integer.toString(w)+"\n" +
                        Integer.toString(k);
                var button = new JButton("");
                button.setActionCommand(acctionCommand);
                button.addActionListener(this::actionPerformed);
                row.add(button);
                button.setPreferredSize(new Dimension(100,100));
                button.setFont(new Font(Font.SERIF, Font.PLAIN,  30));
            }
            toplvl.add(row);
        }
        return toplvl;
    }

    protected void restartGame(){
        ticTacToeMultiplayer.restartGame();
        for(ArrayList<JButton> row:buttonFieldArray2dButton){
            for(JButton button:row) button.setText("");
        }
    }
}




