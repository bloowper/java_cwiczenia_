package zadanie7.testowanie;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.io.Serializable;


public class testMessege implements Serializable {
    String m;
    int a;
    int b;

    public testMessege() {
        m = "";
        a=0;
        b=0;
    }

    public testMessege(String m, int a, int b) {
        this.m = m;
        this.a = a;
        this.b = b;
    }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    @Override
    public String toString() {
        return "testMessege{" +
                "m='" + m + '\'' +
                ", a=" + a +
                ", b=" + b +
                '}';
    }
}
