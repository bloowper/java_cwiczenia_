package zadanie7.testowanie;

import java.io.*;
import java.net.*;
import java.beans.*;

public class test2 {
    public static void main(String[] args) {

        try {
            Socket socket = new Socket("localhost", 2222);
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(socket.getOutputStream()));
            testMessege hej = new testMessege("henlo", 1, 1);
            encoder.writeObject(hej);
            encoder.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
