package zadanie7;

import zadanie6.Status;
import zadanie6.TicTacToeUtilities;

import javax.swing.*;
import java.awt.*;


//TODO synchronizacja rozmiarow plansz!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//za malo czasu przed 12 :(
public class Runner {
    public static void main(String[] args) {
        int startsize=3;
        int winsize=3;
        String ip;
        Integer port;
        try {
            startsize = Math.abs(Integer.parseInt(JOptionPane.showInputDialog("rozmiar planszy")));
            winsize = Math.abs(Integer.parseInt(JOptionPane.showInputDialog("Liczba pol do wygranej")));
            if(winsize>startsize){
                winsize=startsize;
            }
        } catch (NumberFormatException e) {
            System.out.println("Niepoprawny rozmiar. Ustawiono automatycznie rozmiar na 3x3 liczba do wygranej 3");
            startsize = 3;
            winsize = 3;
        } catch (HeadlessException e) {
            e.printStackTrace();
        }

        JFrame frame = new GUI(startsize,winsize);

        //sekcja serwera
        String adressPort = JOptionPane.showInputDialog("adres:port");
        String[] split = adressPort.split(":");
        ip = split[0];
        port = Integer.valueOf(split[1]);
        ((GUI) frame).connectionSimple = new ConnectionSimple(ip,port);
        ((GUI) frame).ticTacToeMultiplayer.setPlayerShape(((GUI) frame).connectionSimple.isSerwer()? Status.X:Status.O);
        ((GUI) frame).ticTacToeMultiplayer.setOppositeShape(TicTacToeUtilities.oppositeShape(((GUI) frame).ticTacToeMultiplayer.yourShape));
        System.out.println("jestes : "+((GUI) frame).ticTacToeMultiplayer.yourShape);
        System.out.println("przeciwnik jest : "+((GUI) frame).ticTacToeMultiplayer.oppositeShape);
        //koniec sekcji serwera


        SwingUtilities.invokeLater((Runnable)frame);
    }
}
