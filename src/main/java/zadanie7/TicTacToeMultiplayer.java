package zadanie7;

import zadanie6.Status;
import zadanie6.TicTacToe;
import zadanie6.notAllowed;
import zadanie7.exceptions.notYourTurnException;
import zadanie6.*;

public class TicTacToeMultiplayer extends TicTacToe {

    Status  yourShape;
    Status lastMove; //TODO pozbyc sie tej zmiennej
    Status oppositeShape;

    public TicTacToeMultiplayer(int size, int won_size){
        super(size,won_size);
    }

    public TicTacToeMultiplayer(int size, int won_size,Status yourShape){
        super(size,won_size);
        setShapes(yourShape);
    }

    @Override
    public void setCell(int w, int k, Status XO) throws notAllowed {
        if(lastMove != yourShape)
            throw new notYourTurnException();
        super.setCell(w, k, yourShape);
        lastMove = TicTacToeUtilities.oppositeShape(yourShape);
    }

    public void setShapes(Status yourShape){
        this.yourShape = yourShape;
        this.oppositeShape = TicTacToeUtilities.oppositeShape(yourShape);
    }

    /**
     *
     * @param status shape that player will have
     */
    //
    public void setPlayerShape(Status status){
        this.yourShape = status;
    }

    public void setOppositeShape(Status status){this.oppositeShape = status;}

    public Status getOppositeShape() {
        return oppositeShape;
    }

    public Status getLastMove() {
        return lastMove;
    }

    public void setLastMove(Status lastMove) {
        this.lastMove = lastMove;
    }

    /**
     * ONLY FOR TEST PURPOSE!!!!!!!!!!!!!!!!!
     * @param status
     */
    public void SETCURRENTSHAPE(Status status){
        this.lastMove = status;
    }


}
