package zadanie3;

import javax.management.BadAttributeValueExpException;
import java.lang.reflect.Array;
import java.util.*;

/**
 * Generic Priority Queue
 * @author Tomasz Orchowski
*
 */

public class PriorityQueue<type> {
    //data
    public ArrayList<LinkedList<type>> queue;
    int[] possiblePriority = new int[]{0,1,2,3,4,5};


    //constructors
    public PriorityQueue() {
        queue = new ArrayList<>();
        for(int priority:possiblePriority){
            queue.add(new LinkedList<type>());
        }
    }

    /**
     * @param data (T)  data
     * @param priority priority of data <b> in range 0-5</b>
     * @throws IllegalArgumentException throws when bad priority
     */
    public void add(type data, int priority) throws IllegalArgumentException {
        if(priority>=possiblePriority.length){
            throw new IllegalArgumentException("priority 0-5");
        }
        queue.get(queue.size()-priority-1).add(data);
    }

    /**
     * @return next element od PriorityQueue or null if empty
     */
    type get(){
        type toReturn = null;
        Iterator<LinkedList<type>> iterator = queue.iterator();
        while (iterator.hasNext()){
            LinkedList<type> next = iterator.next();
            if(!next.isEmpty()) {
                toReturn = next.poll();
                break;
            }
        }
        return toReturn;
    }

}


