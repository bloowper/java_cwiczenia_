package zadanie6;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.awt.event.*;

public class GUI extends JFrame implements Runnable,ActionListener{

    TicTacToe ticTacToe;
    ArrayList<ArrayList<JButton>> buttonFieldArray2dButton;
    int numOfOccupiedFields;
    int size;
    int winSize;
    public GUI(int size, int winsize) throws HeadlessException {
        super();
        this.size = size;
        numOfOccupiedFields=0;
        ticTacToe = new TicTacToe(size,winsize);
    }

    @Override
    public void run() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        JPanel jPanelMain = new JPanel();
        JPanel panelButtonField = new JPanel();
        panelButtonField.setLayout(new GridLayout(size,size));
        buttonFieldArray2dButton = createButtons();
        for(ArrayList<JButton> row : buttonFieldArray2dButton){
            for(JButton button:row){
                panelButtonField.add(button);
            }
        }
        jPanelMain.add(panelButtonField);

        this.getContentPane().add(jPanelMain);
        this.pack();
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //wiersz \n kolumna
        final String actionCommand = e.getActionCommand();
        final String[] split = actionCommand.split("\n");
        var wiersz = Integer.valueOf(split[0]);
        var kolumna = Integer.valueOf(split[1]);
        System.out.printf("wcisnieto %d %d\n",wiersz,kolumna);
        try {
            final Status pleyerMove = ticTacToe.next_pleyer_move;
            ticTacToe.setCell(wiersz,kolumna, pleyerMove);
            buttonFieldArray2dButton.get(wiersz).get(kolumna).setText(pleyerMove.toString());
            numOfOccupiedFields++;
        } catch (notAllowed notAllowedField) {
            System.out.println("To pole jest juz zajete!");
        }
        final Status checkWon = ticTacToe.checkWon();
        if(checkWon!=null){
            numOfOccupiedFields=0;
            System.out.printf("%s WON!",checkWon.toString());
            restartGame();
        }
        if(numOfOccupiedFields == size*size){
            System.out.println("RESTART");
            restartGame();
        }
    }

    private ArrayList<ArrayList<JButton>> createButtons(){
        var toplvl = new ArrayList<ArrayList<JButton>>();
        for(int w=0;w<size;w++){
            var row = new ArrayList<JButton>();
            for(int k=0;k<size;k++){
                String acctionCommand = Integer.toString(w)+"\n" +
                        Integer.toString(k);
                var button = new JButton("");
                button.setActionCommand(acctionCommand);
                button.addActionListener(this::actionPerformed);
                row.add(button);
                button.setPreferredSize(new Dimension(100,100));
                button.setFont(new Font(Font.SERIF, Font.PLAIN,  30));
            }
            toplvl.add(row);
        }
        return toplvl;
    }

    protected void restartGame(){
        ticTacToe.restartGame();
        for(ArrayList<JButton> row:buttonFieldArray2dButton){
            for(JButton button:row) button.setText("");
        }
    }



    public static void main(String[] args) {
        int startsize=3;
        int winsize=3;

        try {
            startsize = Math.abs(Integer.parseInt(JOptionPane.showInputDialog("rozmiar planszy")));
            winsize = Math.abs(Integer.parseInt(JOptionPane.showInputDialog("Liczba pol do wygranej")));
            if(winsize>startsize){
                winsize=startsize;
                JOptionPane.showMessageDialog(null, "Ilosc pol do wygranej nie moze byc wieksza niz rozmiar planszy");
            }
        } catch (NumberFormatException e) {
            System.out.println("Niepoprawny rozmiar. Ustawiono automatycznie rozmiar na 3x3 liczba do wygranej 3");
            startsize = 3;
            winsize = 3;
        } catch (HeadlessException e) {
            e.printStackTrace();
        }

        JFrame frame = new GUI(startsize,winsize);
        SwingUtilities.invokeLater((Runnable)frame);
    }


}


