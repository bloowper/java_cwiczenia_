package zadanie6;
import org.apache.maven.surefire.shared.lang3.tuple.MutablePair;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Iterator;


public class TicTacToe {
    int size;
    int won_size;
    ArrayList<ArrayList<Cell>> field;
    public Status next_pleyer_move;
    //kapsulizacja lezy xD
    ArrayList<ArrayList<MutablePair<Integer, Integer>>> diagonals;
    public TicTacToe(int size) {
        this.size = size;
        this.won_size = size;
        next_pleyer_move = Status.X;
        this.field = new ArrayList<>();
        for(int i=0;i<size;i++){
            ArrayList<Cell> temp = new ArrayList<Cell>();
            for(int j=0;j<size;j++) temp.add(new Cell());
            this.field.add(temp);
        }
        diagonals = TicTacToeUtilities.returnListOfDiagonals(this.size);
    }

    public TicTacToe(int size, int won_size) {
        this.size = size;
        this.won_size = won_size;
        next_pleyer_move = Status.X;
        this.field = new ArrayList<>();
        for(int i=0;i<size;i++){
            ArrayList<Cell> temp = new ArrayList<Cell>();
            for(int j=0;j<size;j++) temp.add(new Cell());
            this.field.add(temp);
        }
        diagonals = TicTacToeUtilities.returnListOfDiagonals(this.size);
    }

    /**
     * @return Status.X or Status.O when win or <strong>null</strong> when no one have won in this round
     *
     */
    public Status checkWon(){
        //wiersze
        for(ArrayList<Cell> row:this.field){
            Iterator<Cell> iterator = row.iterator();
            MutablePair<Status, Integer> counter = new MutablePair<>(iterator.next().status,1);
            while(iterator.hasNext()){
                Cell cell = iterator.next();
                if(cell.status == counter.left) {
                    counter.right++;
                }
                else{
                    counter.left = cell.status;
                    counter.right = 1;
                }
                if(counter.right == this.won_size && counter.left!=Status.CLEAR)
                    return counter.left;
            }
        }

        //sprwadzamy kolumny
        //MAPA JEST KWADRATOWA
        for(int k=0 ; k<size ; k++){
            MutablePair<Status, Integer> counter = new MutablePair<>(this.field.get(0).get(k).status, 1);
            for(int w=1; w<size; w++){
                Cell cell = this.field.get(w).get(k);
                if(cell.getStatus() == counter.left) {
                    counter.right++;
                }
                else{
                    counter.left = cell.getStatus();
                    counter.right = 1;
                }
                if(counter.right == this.won_size && counter.left!=Status.CLEAR) {
                    return counter.left;
                }
            }
        }


        //sprawdzamy przekatne WSZYSTKIE
        Status c_0_0_status = field.get(0).get(0).getStatus();
        for(int i=1;i<size;i++){
            final Status cell_i_i_status = field.get(i).get(i).getStatus();
            if(cell_i_i_status.compareTo(c_0_0_status)!=0 || c_0_0_status == Status.CLEAR) break;
            if(i==field.size()-1) return c_0_0_status;
        }

        Status c_last_0 = field.get(size-1).get(0).status;
        int k = size-1;
        if(c_last_0!=Status.CLEAR)
        for(int w=0;w<size;w++){
            Status status = field.get(w).get(k).status;
            if(status.compareTo(c_last_0)!=0) break;
            if(w==size-1) return c_last_0;
            k--;
        }

        for (ArrayList<MutablePair<Integer, Integer>> diagonal : this.diagonals) {
            //diagonal jest przekatna z lewej strony planszy na prawa
            //by uzysac przekatna z prawej strony na lewa
            //nalezy odjac od drugiej skladowej MutablePair wielkosc planszy
            MutablePair<Status, Integer> leftToRightCounter = new MutablePair<>(getelem(diagonal.get(0)).status,1);
            MutablePair<Status, Integer> rightToLeftCounter = new MutablePair<>(getelem(reflect(diagonal.get(0))).status,1);

            for (int i = 1; i < diagonal.size(); i++) {
                MutablePair<Integer, Integer> diagonalElement = diagonal.get(i);
                Cell leftToRight = this.getelem(diagonalElement);
                Cell rightToLeft = this.getelem(reflect(diagonalElement));

                if(leftToRight.status == leftToRightCounter.left){
                    leftToRightCounter.right++;
                    if(leftToRightCounter.right == this.won_size && leftToRightCounter.left!=Status.CLEAR)
                        return leftToRightCounter.left;
                }
                else {
                    leftToRightCounter.left = leftToRight.status;
                    leftToRightCounter.right=1;
                }

                if(rightToLeft.status == rightToLeftCounter.left && rightToLeftCounter.left!=Status.CLEAR){
                    rightToLeftCounter.right++;
                    if(rightToLeftCounter.right == this.won_size)
                        return rightToLeftCounter.left;
                }
                else {
                    rightToLeftCounter.left = rightToLeft.status;
                    rightToLeftCounter.right=1;
                }

            }
        }

        return null;
    }

    public void setCell(int w,int k,Status XO) throws notAllowed {
        if(field.get(w).get(k).status.compareTo(Status.CLEAR)!=0)
            throw new notAllowed();
        field.get(w).get(k).status = XO;
        if(XO == Status.O)
            this.next_pleyer_move = Status.X;
        else
            this.next_pleyer_move = Status.O;
    }

    //napsiac testy
    public void restartGame(){
        for(ArrayList<Cell> row : field){
            for(Cell c:row) c.restart();
        }
    }

    public void setWon_size(int won_size) {
        this.won_size = won_size;
    }

    private Cell getelem(MutablePair<Integer, Integer> wsp){
        return this.field.get(wsp.left).get(wsp.right);
    }

    /**
     * reflect point horizontal
     * @param wsp to be reflected horizontal(depends on TicTacToe field size)
     * @return reftected wsp
     */
    private MutablePair<Integer, Integer> reflect(MutablePair<Integer, Integer> wsp){
        return new MutablePair<Integer, Integer>(wsp.left,this.size-wsp.right-1);
    }
}

