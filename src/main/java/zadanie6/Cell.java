package zadanie6;

public class Cell {
    Status status;

    public Cell() {
        status = Status.CLEAR;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) throws notAllowed {
        if (this.status == Status.CLEAR)
            this.status = status;
        else
            throw new notAllowed();
    }

    //brak testow
    protected void restart() {
        status = Status.CLEAR;
    }
}
