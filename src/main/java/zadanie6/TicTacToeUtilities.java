package zadanie6;
import org.apache.maven.surefire.shared.lang3.tuple.MutablePair;
import java.util.ArrayList;

public class TicTacToeUtilities {

    /**
     * @param size - size of field
     * @return ArrayList of ArrayList with is diagonal of filed left->right , if wanted right -> left need to substract size of field from the second component of MutablePair
     *
     *
     */
    public static ArrayList<ArrayList<MutablePair<Integer, Integer>>> returnListOfDiagonals(int size){
        ArrayList<ArrayList<MutablePair<Integer, Integer>>> listsOfDiagonals = new ArrayList<>(2 * size - 1);

        for(int startK=0 ; startK<size; startK++){
            ArrayList<MutablePair<Integer, Integer>> pairs = new ArrayList<>();
            for(int k=startK,w=0;w<size && k<size;w++,k++){
                pairs.add(new MutablePair<>(w,k));
            }
            if(pairs.size()>0)
                listsOfDiagonals.add(pairs);
        }

        for(int startW=1; startW<size; startW++){
            ArrayList<MutablePair<Integer, Integer>> pairs = new ArrayList<>();
            for(int w=startW,k=0;w<size && k<size; w++, k++){
                pairs.add(new MutablePair<>(w,k));
            }
            if(pairs.size()>0)
                listsOfDiagonals.add(pairs);
        }

        return listsOfDiagonals;
    }

    public static Status oppositeShape(Status shape){
        if(shape == Status.X)
            return Status.O;
        else
            return Status.X;
    }

}

class coordinatesAccepter{
    int size;
    public coordinatesAccepter(int size) {
        this.size = size;
    }

    public boolean accept(int w,int k){
        return w<size && w > 0 && k<size && k>0;
    }
}

