package zadanie10;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.TypeVariable;
import java.sql.DriverManager;
import java.util.Arrays;
import java.util.Vector;

public class reflectInfo {
    public static void main(String[] args) {
        if(args.length < 1){
            System.out.println("wymagane podanie parametrow");
            return;
        }


        try {
            Class<?> aClass = Class.forName(args[0]);
            System.out.printf("Class name: %s\n\n",aClass.getSimpleName());
            System.out.printf("Class modifiers: %s\n\n",Modifier.toString(aClass.getModifiers()));

        //FIELDS
            Field[] fields = aClass.getDeclaredFields();
            System.out.printf("Class fields:\n");

            for (Field field : fields) {
                String modifer = Modifier.toString(field.getModifiers());
                Class<?> fieldType = field.getType();
                String fieldName = field.getName();

                System.out.println("\t->"+modifer+" "+fieldType.getSimpleName()+" "+fieldName);
            }

        //METHODS
            Method[] methods = aClass.getDeclaredMethods();
            System.out.println("Class methods:");
            for (Method method : methods) {
                String modifer = Modifier.toString(method.getModifiers());
                String returnType = method.getReturnType().getSimpleName();
                String name = method.getName();
                StringBuilder stringBuilder = new StringBuilder();
                Arrays.stream(method.getParameterTypes()).map(Class::getSimpleName).forEach(stringBuilder::append);
                String arguments = stringBuilder.toString();

                stringBuilder = new StringBuilder();//nie znalazlem metody do czyszczenia :(
                Arrays.stream(method.getExceptionTypes()).map(Class::getSimpleName).forEach(stringBuilder::append);
                String exceptions = stringBuilder.toString();


                System.out.println("\t->"+modifer+" "+returnType+" "+name+"("+arguments+")"+" throws "+exceptions);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
