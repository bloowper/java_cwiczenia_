package kolokwium2;

import java.io.*;

// tu mi sie pomieszaly wyjatki z plimem z tymi powiazanymi za rozlaczenie polaczenia....
// ehhh
public class Serwer implements Runnable{
    ConnectionObject<Message> connectionObject;


    public Serwer(String ip, int port) {
        try {
            System.out.println("Oczekiwanie na klienta");
            connectionObject = new ConnectionObject<>(ip,port);
            System.out.println("nawiazano polaczenie");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void run() {
        while (true){

            try {
                String zapytanie = connectionObject.readObject().getZapytanie();

                File file = new File(zapytanie);
                if(!file.exists()){
                    Message message = new Message();
                    message.setOdpowiedz("Plik nie istnieje\n");
                    connectionObject.writeObject(message);
                    continue;
                }

                try(var br = new BufferedReader(new FileReader(file))){

                    StringBuilder stringBuilder = new StringBuilder();
                    String s;

                    try {
                        while ((s=br.readLine())!=null){
                            stringBuilder.append(s);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    String s1 = stringBuilder.toString();
                    Message message = new Message();
                    message.setOdpowiedz(s1);
                    connectionObject.writeObject(message);

                }

            }
            catch (EOFException e){
                System.out.println("zakonczono polaczenie z kilentem");
                break;
            }
            catch (IOException e) {
                e.printStackTrace();
                break;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

        }
    }
}

/**
 *
 *         args[0] - ip
 *         args[1] - port
 */
class SerwerRunner{
    public static void main(String[] args) {
        new Thread(new Serwer(args[0],Integer.parseInt(args[1]))).start();

    }
}
