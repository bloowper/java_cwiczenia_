package kolokwium2;

import java.io.Serializable;

public class Message implements Serializable {
    String zapytanie;
    String odpowiedz;

    public Message() {
    }

    public String getZapytanie() {
        return zapytanie;
    }

    public void setZapytanie(String zapytanie) {
        this.zapytanie = zapytanie;
    }

    public String getOdpowiedz() {
        return odpowiedz;
    }

    public void setOdpowiedz(String odpowiedz) {
        this.odpowiedz = odpowiedz;
    }
}
