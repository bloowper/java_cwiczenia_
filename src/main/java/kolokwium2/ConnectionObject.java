package kolokwium2;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

// zarowno klient jak i serwer moze wystartowac pierwszy
// z racji ze jest to polaczenie p2p to stwierdzilem ze tak bedzie ok


public class ConnectionObject<T extends Serializable>{
    Socket socket;
    ObjectOutputStream objectOutputStream;
    ObjectInputStream objectInputStream;

    /**
     * Locl thread when wating for connection, writing reading object lock thread to.
     * @param ip ip connection
     * @param port port
     * @throws IOException
     */
    ConnectionObject(String ip, int port) throws IOException {
        try {
            socket = new Socket(ip,port);
        } catch (IOException e) {

            try {
                ServerSocket serverSocket = new ServerSocket(port);
                socket = serverSocket.accept();

                OutputStream outputStream = socket.getOutputStream();
            } catch (IOException ioException) {
                throw ioException;
            }


        }

        objectOutputStream = objectOutputStream();
        objectInputStream = objectInputStream();


    }

    private ObjectOutputStream objectOutputStream() throws IOException {
        return new ObjectOutputStream(socket.getOutputStream());
    }


    private ObjectInputStream objectInputStream() throws IOException {
        return new ObjectInputStream(socket.getInputStream());
    }


    /**
     * @param obj obj to write to socket
     * @throws IOException when connection is closed on one or both side
     */
    void writeObject(T obj) throws IOException {
        objectOutputStream.writeObject(obj);
    }

    /**
     * blokuje watek do momentu odczytania obiektu z strumienia
     * @return T or null when no object in stream
     * @throws IOException
     * @throws EOFException occurs when one of ObjectStream closed (end of connection)
     */
    T readObject() throws IOException, EOFException, ClassNotFoundException {
        Object o = objectInputStream.readObject();
        return (T)o;
    }

    protected void finalize() throws Throwable{
        objectInputStream.close();
        objectOutputStream.close();
        socket.close();
    }

    public void close() throws IOException {
        objectOutputStream.close();;
        objectInputStream.close();
        socket.close();
    }
}


// Odpalamy ten obiekt dwa razy, wymagane ustawienia pozwolenia na Parrarel run.
class runner{
    public static void main(String[] args) throws IOException {
        ConnectionObject<String> connectionObject = new ConnectionObject<>("localhost",44444);
        new Thread(() -> {

            while (true) {

                try {
                    System.out.println("przed odebraniem obiektu");
                    String s = connectionObject.readObject();
                    System.out.println("po odebraniem obiektu");

                    System.out.println(s);
                }
                catch (EOFException e ){
                    System.out.println("Druga strona zakonczyla polaczenie");
                    break;
                }
                catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

            }

        }).start();



        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String temp;
        while ((temp = reader.readLine())!=null){
            connectionObject.writeObject(temp);
        }

    }
}

