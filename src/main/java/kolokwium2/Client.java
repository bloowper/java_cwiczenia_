package kolokwium2;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class Client extends JFrame implements Runnable {

    JEditorPane jEditorPane;
    JTextField jTextField;
    JButton jButton;
    String ip;
    int port;
    ConnectionObject<Message> connectionObject;
    public Client(String ip,String port) throws HeadlessException, IOException {
        super();
        this.ip = ip;
        this.port = Integer.parseInt(port);
        new Thread(() -> {

            try {
                connectionObject = new ConnectionObject<>(this.ip,this.port);
            } catch (IOException e) {
             JOptionPane.showMessageDialog(null,"Problem z polaczeniem","Error",JOptionPane.ERROR_MESSAGE);
            }

        }).start();

    }

    @Override
    public void run() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new GridBagLayout());
        setLocationRelativeTo(null);
        GridBagConstraints gridBagConstraints = new GridBagConstraints();



        //creating top panel
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BoxLayout(jPanel,BoxLayout.X_AXIS));
        jTextField = new JTextField();
        jTextField.setPreferredSize(new Dimension(400,30));
        jButton = new JButton("Open");
        jButton.addActionListener(new buttonListener());
        jPanel.add(jButton);
        jPanel.add(jTextField);
        gridBagConstraints.gridy=0;gridBagConstraints.gridx=0;
        this.getContentPane().add(jPanel);



        jEditorPane = new JEditorPane();
        jEditorPane.setEditable(false);
        jEditorPane.setPreferredSize(new Dimension(400,400));
        jEditorPane.setText("Nie wybrano jeszcze:");
        gridBagConstraints.gridy=1;gridBagConstraints.gridx=0;
        getContentPane().add(jEditorPane,gridBagConstraints);


        //w ramach testow :)
        jTextField.setText("src/main/java/kolokwium2/przykladowy.txt");

        pack();
        setVisible(true);
    }


    class buttonListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            if (connectionObject!=null){
                Message message = new Message();
                String text = jTextField.getText();
                message.setZapytanie(text);


                try {
                    connectionObject.writeObject(message);
                    new Thread(new downloader()).start();

                } catch (IOException ioException) {
                    JOptionPane.showMessageDialog(null,"Problem z polaczeniem","Error",JOptionPane.ERROR_MESSAGE);
                }

            }else {
                JOptionPane.showMessageDialog(jButton,"nie jestes jeszcze polaczony","Error",JOptionPane.ERROR_MESSAGE);

            }
        }
    }

    class downloader implements Runnable{
        @Override
        public void run() {

                try {
                    Message message = connectionObject.readObject();
                    System.out.println("Odebrano : "+message.getOdpowiedz());
                    jEditorPane.setText(message.getOdpowiedz());
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }


        }
    }
}


/**
 *
 *         args[0] - ip
 *         args[1] - port
 */
class klietRunner{

    public static void main(String[] args) throws IOException {

        Client client = new Client(args[0],args[1]);
        SwingUtilities.invokeLater(client);
    }
}
