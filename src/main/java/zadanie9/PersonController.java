package zadanie9;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class PersonController {
    private PersonModel personModel;
    private PersonView personView;
    private DateBase connecter;

    //zmienne do JOptionPane - Edytowanie - dla przycisku edytuj
    JTextField jTextFieldEditFirstName;
    JTextField jTextFieldEditLastName;
    JTextField jTextFieldEditPhoneNumber;

    public PersonController(PersonModel personModel, PersonView personView) {
        this.personModel = personModel;
        this.personView = personView;
        this.personView.setID(personModel.getId());
        this.personView.setyFirstName(personModel.getFirstName());
        this.personView.setLastName(personModel.getLastName());
        this.personView.setPhoneNumber(personModel.getPhoneNumber());
        personView.setEditListener(new EditListener());
        personView.setDeleteListener(new DeleteListener());

        jTextFieldEditFirstName = new JTextField();
        jTextFieldEditLastName = new JTextField();
        jTextFieldEditPhoneNumber = new JTextField();

        jTextFieldEditFirstName.setText(personModel.getFirstName());
        jTextFieldEditLastName.setText(personModel.getLastName());
        jTextFieldEditPhoneNumber.setText(personModel.getPhoneNumber());

    }

    //Czy kontroler moze posiadac getery?
    public PersonModel getPersonModel() {
        return personModel;
    }

    public PersonView getPersonView() {
        return personView;
    }

    //TODO to nie jest dobre rozwiazanie zeby controler mial wglad w CALA baze danych
    // chyba wymagana refaktoryzacja calego kodu
    public void setConnecter(DateBase connecter){this.connecter=connecter;}


    //TODO
    class EditListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            Object [] fields = {
                    "imie",jTextFieldEditFirstName,
                    "nazwisko",jTextFieldEditLastName,
                    "numer",jTextFieldEditPhoneNumber
            };

            int dialog = JOptionPane.showConfirmDialog(personView, fields, "this is a header", JOptionPane.OK_CANCEL_OPTION);
            if(dialog == JOptionPane.OK_OPTION){
                String firstNameNew = jTextFieldEditFirstName.getText();
                String lastNameNew = jTextFieldEditLastName.getText();
                String phoneNumberNew = jTextFieldEditPhoneNumber.getText();

                personModel.setFirstName(firstNameNew);
                personModel.setLastName(lastNameNew);
                personModel.setPhoneNumber(phoneNumberNew);

                personView.setyFirstName(firstNameNew);
                personView.setLastName(lastNameNew);
                personView.setPhoneNumber(phoneNumberNew);

                try {
                    connecter.updateModel(personModel);
                } catch (SQLException throwables) {
                    JOptionPane.showMessageDialog(personView,"Problem with date base update","Error",JOptionPane.ERROR_MESSAGE);
                }

            }
        }
    }

    //TODO pozbyc sie uzyc tej metody i usunac
    /*
    * NOT IN USE!!!!!!!!!!!!!!!!!!
    * DO NOT USE!!!!!!!!!!!!!!!!!!
     */
    class DeleteListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            personView.setyFirstName("Usunieto :)");
            personView.setLastName("Usunieto :)");
            personView.setPhoneNumber("Usunieto :)");
        }
    }


}


/**
 * For "Graphic testing" purpose only
 */
class PersonControllerTesterGraphic{
    public static void main(String[] args) {
        JFrame jFrame = new JFrame("testowanie");
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.setSize(new Dimension(900,900));
        jFrame.getContentPane().setLayout(new GridBagLayout());

        //element ktory chcemy sobie obejrzec graficznie :)
        PersonModel personModel = new PersonModel("Stefan","Banach","235711");
        PersonController controller = new PersonController(personModel, new PersonView());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx=1;constraints.gridy=1;
        jFrame.add(controller.getPersonView(),constraints);

        //stworzmy sobie jakiesz otoczenie tego elementu
        for(int w=0;w<=2;w++){
            for(int k=0;k<=2;k++){
                if(!(w==1 && k == 1)){

                    constraints.gridx = k;
                    constraints.gridy = w;
                    jFrame.getContentPane().add(new JButton("⛇⛇⛇ Otoczenie ⛇⛇⛇"), constraints);
                }
            }
        }

        jFrame.pack();
        jFrame.setVisible(true);
    }
}