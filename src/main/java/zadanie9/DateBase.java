package zadanie9;
import java.io.File;
import java.sql.*;
import java.lang.*;
import java.util.Vector;

// TODO zmienic na try with resources bo nie zamykam statementow
// TODO nazwe tabeli z ktorej korzystam przydalo by sie trzymac jako pole obiektu.


public class DateBase {
    Connection connection;


    public DateBase() throws SQLException, ClassNotFoundException {
        Class.forName("org.sqlite.JDBC");
        connection = DriverManager.getConnection("jdbc:sqlite:file:src/main/java/zadanie9/phonebook.db","SA","");
    }

    /**
     *
     * @param file path to MySql Datebase file
     * @throws ClassNotFoundException when drivers for DB are not available
     * @throws SQLException
     */
    public DateBase(File file) throws ClassNotFoundException, SQLException {
        Class.forName("org.sqlite.JDBC");
        connection = DriverManager.getConnection("jdbc:sqlite:file:"+file.toPath(),"SA","");

        // na wypadek otiwerania bazy danych ktora zostanie dopiero utoworzona.
        // tworzymy potrzebna tabele na dae
        //TODO rozbic tego typu twory i zamykac statemate
        connection.createStatement().executeUpdate(
                "CREATE TABLE IF NOT EXISTS book (" +
                        "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "firstName VARCHAR(30)," +
                        "lastName VARCHAR(30)," +
                        "phoneNumber VARCHAR(30)" +
                        ")"
        );

    }

    /**
     * @return return vector of PersonModels with datebase related id
     * @throws SQLException
     */
    Vector<PersonModel> getModels() throws SQLException {
        Vector<PersonModel> models = new Vector<>(30);
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT * FROM book");
        while (resultSet.next()){
            int id = resultSet.getInt("id");
            String firstName = resultSet.getString("firstName");
            String lastName = resultSet.getString("lastName");
            String phoneNumber = resultSet.getString("phoneNumber");
            models.add(new PersonModel(id,firstName,lastName,phoneNumber));
        }
        statement.close();
        return models;
    }

    /**
     * @param model model in datebase with same ID number will be updated
     * @throws SQLException
     */
    void updateModel(PersonModel model) throws SQLException {
        String statement = "UPDATE book SET " +
                "firstName = "+"'"+model.getFirstName()+"',"+
                "lastName = '"+model.getLastName()+"',"+
                "phoneNumber = '"+model.getPhoneNumber()+"' "+
                "WHERE "+
                "id = " + model.getId();
        System.out.println(statement);
        connection.createStatement().executeUpdate(statement);
    }

    /**
     * Deleteing model in datebase (model to delete will be recognize be id)
     * @param model model to be deleted
     * @throws SQLException
     */
    void deleteModel(PersonModel model) throws SQLException {

        String stat = "DELETE FROM book WHERE id = " + model.getId();
        System.out.println(stat);
        Statement statement = connection.createStatement();
        statement.executeUpdate(stat);
    }

    /**
     * @param model model that will be added to datebse
     * @return ID of releted model in datebase ( id of new model in DB )
     * @throws SQLException
     */
    int addModel(PersonModel model) throws SQLException {

        String statementContent = "INSERT INTO book " +
                "(firstName, lastName, phoneNumber) VALUES " +
                "( '"+model.getFirstName()+"', '"+ model.getLastName()+"' ,'"+model.getPhoneNumber()+"');";
        System.out.println(statementContent);
        connection.createStatement().executeUpdate(statementContent);

        String QuerryForId = String.format("SELECT id FROM book WHERE firstName = '%s' AND lastName = '%s' AND phoneNumber = '%s'", model.getFirstName(),model.getLastName(),model.getPhoneNumber());
        System.out.println(QuerryForId);
        ResultSet resultSet = connection.createStatement().executeQuery(QuerryForId);
        return resultSet.getInt("id");
    }


    @Override
    protected void finalize() throws Throwable {
        //tutaj bede zamykac polaczenie z baza danych, dopiero gdy garbage colector bedzie usowac klase
        // nie wiem czy to jest dobry pomysl
        // natomiast nie chcialem otwierac nowego polaczenia przy kazdym zapytaniu
        connection.close();
        super.finalize();
    }

}


/**
 * Only for early testing purpose
 * CLASS TO DELET
 */
class debugPersonDbLoader {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {

        DateBase personDbConnecter = new DateBase(new File("src/main/java/zadanie9/phonebook.db"));
        System.out.println(personDbConnecter.getClass());
        Vector<PersonModel> models = personDbConnecter.getModels();
        models.forEach(System.out::println);

        PersonModel model = new PersonModel(1, "f", "f", "3123214123214234");

        personDbConnecter.updateModel(model);

        Vector<PersonModel> models1 = personDbConnecter.getModels();
        models1.forEach(System.out::println);

        personDbConnecter.deleteModel(model);

        Vector<PersonModel> models2 = personDbConnecter.getModels();
        models2.forEach(System.out::println);


        personDbConnecter.addModel(new PersonModel("NOWY", "NOWY", "666 666 666"));
        Vector<PersonModel> models3 = personDbConnecter.getModels();
        models3.forEach(System.out::println);




    }
}