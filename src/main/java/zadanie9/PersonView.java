package zadanie9;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class PersonView extends JPanel {
    //nie wiem czy taki sposob deklarowania zmiennych nie jest jakos super bardzo niezgodnty z konwencja
    //natomiast zastanawialem sie chwile i uznalem ze w ten sposob
    //szybciej mi bedzie lapac sie w zmiennych dotyczacych danych jElementow :)

    JLabel jLabelID;            int xID=0;              int yID=0;
    JLabel jLabelFirstName;     int xFirstName =1;      int yFirstName =0;
    JLabel jLabelLastName;      int xLastName =2;       int yLastName =0;
    JLabel jLabelPhoneNumber;   int xPhoneNumber =3;    int yPhoneNumber =0;
    JButton jButtonEdit;        int xEdit =4;           int yEdit =0;           String editName = "Edit";
    JButton jButtonDelete;      int xDelete =5;         int yDelete =0;         String deleteName = "Delete";
    JCheckBox jCheckBox;        int xCheckBox=6;        int yCheckBox=0;
    int ipadx = 10;
    int ipady = 3;
    float ALIGMENT_X = Component.RIGHT_ALIGNMENT;

    public PersonView() {
        super();
        jLabelID = new JLabel("Empty ID");
        jLabelFirstName = new JLabel("EMPTY name");
        jLabelLastName = new JLabel("EMPTY sname");
        jLabelPhoneNumber = new JLabel("EMPTY number");
        jButtonEdit = new JButton(editName);
        jButtonDelete = new JButton(deleteName);
        jCheckBox = new JCheckBox();

        GridBagLayout layout = new GridBagLayout();
        setLayout(layout);
        setAlignmentX(ALIGMENT_X);

        GridBagConstraints constraints = new GridBagConstraints();
        constraints.ipadx = ipadx;
        constraints.ipady = ipady;

    //JlabelID
        constraints.gridx = xID;
        constraints.gridy = yID;
        add(jLabelID,constraints);
    //jLabelFirstName
        constraints.gridx = xFirstName;
        constraints.gridy = yFirstName;
        add(jLabelFirstName,constraints);
    //jLabelLastName
        constraints.gridx = xLastName;
        constraints.gridy = yLastName;
        add(jLabelLastName,constraints);
    //jLabelPhoneNumber
        constraints.gridx = xPhoneNumber;
        constraints.gridy = yPhoneNumber;
        add(jLabelPhoneNumber,constraints);
    //jButtonEdit
        constraints.gridx = xEdit;
        constraints.gridy = yEdit;
        jButtonEdit.setHorizontalAlignment(10);
        add(jButtonEdit,constraints);
    //jCheckBox
        constraints.gridx = xCheckBox;
        constraints.gridy = yCheckBox;
        add(jCheckBox,constraints);


    }

    public void setDeleteListener(ActionListener listener){

        jButtonDelete.addActionListener(listener);
    }

    public void setEditListener(ActionListener listener){

        jButtonEdit.addActionListener(listener);
    }

    public void setID(int ID){jLabelID.setText(String.valueOf(ID));}

    public void setyFirstName(String firstName){
        jLabelFirstName.setText(firstName);
    }

    public void setLastName(String lastName){
        jLabelLastName.setText(lastName);
    }

    public void setPhoneNumber(String phoneNumber){

        jLabelPhoneNumber.setText(phoneNumber);
    }

    void displayErrorMessage(String errorMessage){

        JOptionPane.showMessageDialog(this, errorMessage);
    }

    boolean isSelected(){
        return jCheckBox.isSelected();
    }

}


/**
 * For "Graphic testing" purpose only
 */
class PersonViewTesterGraphic  {
    public static void main(String[] args) {
        JFrame jFrame = new JFrame("testowanie");
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.setSize(new Dimension(900,900));
        jFrame.getContentPane().setLayout(new GridBagLayout());

        //element ktory chcemy sobie obejrzec graficznie :)

        PersonView personView = new PersonView();
        personView.setEditListener(e -> {
            System.out.println("wcisnieto przycisk edycji");
            personView.setyFirstName("Imie");
            personView.setLastName("Nazwisko");
            personView.setPhoneNumber("3252423424252");
        });

        personView.setDeleteListener(e -> {
            System.out.println("wcysnieto przycisk delete");
            personView.displayErrorMessage("Pojawia sie error");
        });
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;gridBagConstraints.gridy=1;
        jFrame.getContentPane().add(personView,gridBagConstraints);

        //stworzmy sobie jakiesz otoczenie tego elementu
        for(int w=0;w<=2;w++){
            for(int k=0;k<=2;k++){
                if(!(w==1 && k == 1)){
                    GridBagConstraints constraints = new GridBagConstraints();
                    constraints.gridx = k;
                    constraints.gridy = w;
                    jFrame.getContentPane().add(new JButton("⛇⛇⛇ Otoczenie ⛇⛇⛇"), constraints);
                }
            }
        }

        jFrame.pack();
        jFrame.setVisible(true);
    }
}

class PersonViewTesterGraphicv2 extends JFrame{

    public static void main(String[] args) {

    }
}
