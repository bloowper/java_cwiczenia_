package zadanie9;

//Swing doprowadzil mnie do czerwonej goraczki
//po prostdu chce skonczyc to zadanie ;-; juz mniejsza
//o zgrabnosc rozwiazania ;--------;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Vector;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


//TODO 1
// poprawic jakos alligment personview zeby byly wyrownanie do prawej

//TODO 2
// dodac sprawdzanie wprowadzanych danych tzn zeby numer byl numerem ect


//BUG 1
// mozliwosc zaladowania nowej bazy danych bez wyczyszczenia kontenera WIDOKOW z poprzedniej bazy danych
// zludne wrazenie addytywnosci dla uzytkownika.

public class GUI extends JFrame implements Runnable {

    GridBagConstraints constraints;
    JMenuBarGUI menuBar;


    DateBase dateBase;
    JPanel personViewList; // Pojemnik dla view osob
    Vector<PersonController> personControllers;

    public GUI() throws HeadlessException {
        super();
        constraints = new GridBagConstraints();
    }

    @Override
    public void run() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(900,900);
        setLayout(new GridBagLayout());

        personControllers = new Vector<>();
        personViewList = new JPanel();
        BoxLayout layoutPersonViewList = new BoxLayout(personViewList, BoxLayout.Y_AXIS);
        personViewList.setLayout(layoutPersonViewList);

        constraints.gridx=1;constraints.gridy=1;
        getContentPane().add(personViewList,constraints);

        menuBar = new JMenuBarGUI();
        this.setJMenuBar(menuBar);




        //pack();
        setVisible(true);
    }


    //TODO
    // ogarnac odrobine kolejnosc inicjalizacji obiektow
    // w zalozeniu chcialem jew inicjalizowac w miejscu odpowiednim do polozenia w JmenuBarze
    // ale chyba to nie zdaje swojego zadania, tzn mozliwe bedzie uzycie zasobow nie zainicjalizowanych

    //TODO
    // zakladki w jmenu zaimplementowac jako inner class.
    // bylo by to troche bardziej czytelne imo
    class JMenuBarGUI extends  JMenuBar{
        JMenu file;
            JMenuItem fileDateBaseLoad;
            JMenuItem fileCreateNewDataFile;
            final JFileChooser jFileChooser = new JFileChooser("src/main/java/zadanie9");
        JMenu edit;
            JMenuItem editDelete;
            JMenuItem editAdd;

        public JMenuBarGUI() {
            super();
            file = new JMenu("File");
            add(file);
            fileDateBaseLoad = new JMenuItem("Load data base");
            fileCreateNewDataFile = new JMenuItem("Create new data base");
            file.add(fileDateBaseLoad);
            file.add(fileCreateNewDataFile);
    //FILE
    //FILE/LOAD DATE BASE
            FileNameExtensionFilter filter = new FileNameExtensionFilter("date base", "db","DB");
            jFileChooser.setFileFilter(filter);
            fileDateBaseLoad.addActionListener(e -> {

                jFileChooser.setFileSelectionMode(JFileChooser.OPEN_DIALOG);
                int dialog = jFileChooser.showOpenDialog(this);
                if(dialog == JFileChooser.APPROVE_OPTION){

                    try {
                        File selectedFile = jFileChooser.getSelectedFile();
                        System.out.println(selectedFile);
                        dateBase = new DateBase(selectedFile);
                        Vector<PersonModel> models = dateBase.getModels();
                        models.forEach(model -> {
                            System.out.println(model);
                            PersonView personView = new PersonView();
                            PersonController controller = new PersonController(model, personView);
                            controller.setConnecter(dateBase);
                            personControllers.add(controller);
                            personViewList.add(personView);

                        });
                        SwingUtilities.updateComponentTreeUI(personViewList);
                    } catch (ClassNotFoundException | SQLException classNotFoundException) {
                        JOptionPane.showMessageDialog(jFileChooser,"Problem with date base file","Error",JOptionPane.ERROR_MESSAGE);
                    }

                }

            });

    //FILE/CREATE NEW DATA BASe
            fileCreateNewDataFile.addActionListener(e -> {
                int result = jFileChooser.showSaveDialog(fileCreateNewDataFile);
                if (result == JFileChooser.APPROVE_OPTION) {

                    try {
                        File selectedFile = jFileChooser.getSelectedFile();
                        File selectedFileDBextension;
                        if(!selectedFile.getAbsoluteFile().toString().endsWith(".db")){
                            selectedFileDBextension = new File(selectedFile.getAbsoluteFile()+".db");
                        }else {
                            selectedFileDBextension = selectedFile;
                        }
                        dateBase = new DateBase(selectedFileDBextension);

                    } catch (ClassNotFoundException | SQLException classNotFoundException) {
                        JOptionPane.showMessageDialog(jFileChooser,"Problem with creating date base file","Error",JOptionPane.ERROR_MESSAGE);
                    }
                }

            });


    //EDIT
            edit = new JMenu("Edit");
            add(edit);
            editDelete = new JMenuItem("Delete selected");
            edit.add(editDelete);
    //EDIT/DELETE SELECTED
            editDelete.addActionListener(e -> {

                int dialog = JOptionPane.showConfirmDialog(this, "Are you sure", "Delet", JOptionPane.YES_NO_OPTION);
                if(dialog == JOptionPane.OK_OPTION){
                    System.out.println("Deleting elements pressed");
                    var collect = personControllers.stream().filter(personController -> personController.getPersonView().isSelected()).collect(Collectors.toCollection(Vector::new));
                    collect.forEach(personController -> System.out.println("Delete : "+personController.getPersonModel()));
                    for (PersonController controller : collect) {
                        personViewList.remove(controller.getPersonView());
                        SwingUtilities.updateComponentTreeUI(personViewList);
                        try {
                            dateBase.deleteModel(controller.getPersonModel());
                        } catch (SQLException throwables) {
                            JOptionPane.showMessageDialog(edit,"unable to delete object from date base");
                        }
                    }
                    personControllers.removeAll(collect);

                }


            });
    //EDIT/ADD NEW
            editAdd = new JMenuItem("Add new");
            edit.add(editAdd);
            editAdd.addActionListener(e -> {

                if (dateBase != null) {
                    JTextField firstName = new JTextField();
                    JTextField secondName = new JTextField();
                    JTextField phoneNumbr = new JTextField();
                    Object [] fields = {
                            "Name",firstName,
                            "Last name",secondName,
                            "Phone number",phoneNumbr
                    };


                    int dialog = JOptionPane.showConfirmDialog(this, fields, "Enter data", JOptionPane.OK_CANCEL_OPTION);
                    if(dialog == JOptionPane.OK_OPTION){
                        String firstNameText = firstName.getText();
                        String secondNameText = secondName.getText();
                        String phoneNumbrText = phoneNumbr.getText();
                        //TODO sprawdzic dane wejsciowe


                        String numberPattern = "[\\d, ]{6,15}";
                        Pattern pattern = Pattern.compile(numberPattern);

                        if( ! pattern.matcher(phoneNumbrText).matches()){
                            JOptionPane.showMessageDialog(editAdd,"the number format is incorret","Validation problem",JOptionPane.ERROR_MESSAGE);

                        }
                        else {

                            try {
                                PersonModel personModel = new PersonModel(firstNameText, secondNameText, phoneNumbrText);
                                int id = dateBase.addModel(personModel);
                                personModel.setId(id);
                                PersonView personView = new PersonView();
                                PersonController controller = new PersonController(personModel,personView);
                                System.out.println("added : "+controller.getPersonModel());//informacje do konsoli
                                personViewList.add(controller.getPersonView());
                                personControllers.add(controller);
                                SwingUtilities.updateComponentTreeUI(this);
                            } catch (SQLException throwables) {
                                JOptionPane.showMessageDialog(editAdd,"Problem with adding new person","Date base error",JOptionPane.ERROR_MESSAGE);
                            }
                        }

                    }
                } else {
                    JOptionPane.showMessageDialog(menuBar,"Load date base");

                }

            });

        }
    }

}


class test{
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        new DateBase(new File("src/main/java/zadanie9/phonebook2.db"));

    }
}