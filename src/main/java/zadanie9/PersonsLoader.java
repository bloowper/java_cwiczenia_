package zadanie9;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Vector;

public class PersonsLoader {
    private File file;

    public PersonsLoader(File file){
        this.file = file;

    }
    public Vector<PersonModel> loadFromFile() throws FileNotFoundException {

        XMLDecoder xmlDecoder = new XMLDecoder(new FileInputStream(file));
        Object o = null;
        try {
            o = xmlDecoder.readObject();
        } catch (ArrayIndexOutOfBoundsException e) {
            return new Vector<>(50);
        }
        return (Vector<PersonModel>)o;
    }

    public void saveToFile(Vector<PersonModel> personModels) throws FileNotFoundException {
        XMLEncoder encoder = new XMLEncoder(new FileOutputStream(file));
        encoder.writeObject(personModels);
        encoder.flush();
        encoder.close();
    }
}









