package Testowanie.swing;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class ConnectionObject<T extends Serializable>{
    Socket socket;
    ObjectOutputStream objectOutputStream;
    ObjectInputStream objectInputStream;

    /**
     * Locl thread when wating for connection, writing reading object lock thread to.
     * @param ip ip connection
     * @param port port
     * @throws IOException
     */
    ConnectionObject(String ip, int port) throws IOException {
        try {
            socket = new Socket(ip,port);
        } catch (IOException e) {

            try {
                ServerSocket serverSocket = new ServerSocket(port);
                socket = serverSocket.accept();

                OutputStream outputStream = socket.getOutputStream();
            } catch (IOException ioException) {
                throw ioException;
            }


        }

        objectOutputStream = objectOutputStream();
        objectInputStream = objectInputStream();


    }

    private ObjectOutputStream objectOutputStream() throws IOException {
        return new ObjectOutputStream(socket.getOutputStream());
    }


    private ObjectInputStream objectInputStream() throws IOException {
        return new ObjectInputStream(socket.getInputStream());
    }


    /**
     * @param obj obj to write to socket
     * @throws IOException when connection is closed on one or both side
     */
    void writeObject(T obj) throws IOException {
        objectOutputStream.writeObject(obj);
    }

    /**
     * blokuje watek do momentu odczytania obiektu z strumienia
     * @return T or null when no object in stream
     * @throws IOException
     * @throws EOFException occurs when one of ObjectStream closed (end of connection)
     */
    T readObject() throws IOException, EOFException, ClassNotFoundException {
        Object o = objectInputStream.readObject();
        return (T)o;
    }

    protected void finalize() throws Throwable{
        objectInputStream.close();
        objectOutputStream.close();
        socket.close();
    }

    public void close() throws IOException {
        objectOutputStream.close();;
        objectInputStream.close();
        socket.close();
    }
}
