package Testowanie.swing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class buttonTest extends JFrame implements Runnable {
    public buttonTest() throws HeadlessException {
        super();

    }
    @Override
    public void run() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setLayout(new FlowLayout());

        JButton jButton1 = new JButton("Guzik 1");
        JButton jButton2 = new JButton("Guzik 2");
        JButton jButton3 = new JButton("Guzik 3");

        Listener listener = new Listener();//to jest ta JEDYNA instancja obiektu listener ktora bedzie nasluchiwac

        jButton1.addActionListener(listener);
        jButton2.addActionListener(listener);
        jButton3.addActionListener(listener);

        jButton1.setActionCommand("Jestem guzik nr 1");
        jButton2.setActionCommand("Jestem guzik nr 2");
        jButton3.setActionCommand("Jestem guzik nr 3");


        getContentPane().add(jButton1);
        getContentPane().add(jButton2);
        getContentPane().add(jButton3);

        //do guzika mozemy przypisac tez wiecej niz tylko jeden action listener
        Listener2 listener2 = new Listener2();
        jButton1.addActionListener(listener2);
        jButton2.addActionListener(listener2);



        pack();
        setVisible(true);
    }

    class Listener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String actionCommand = e.getActionCommand();
            System.out.println("Listener nr 1: Wcisnieto guzik z komenda :: "+actionCommand);
            //na podstawie getActionCommand rozpoznajemy ktory guzik wywolal tego konkretnego action listenera
        }
    }

    class Listener2 implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println("Listener nr 2");
        }
    }

}


class runner{
    public static void main(String[] args) {
        buttonTest buttonTest = new buttonTest();
        SwingUtilities.invokeLater(buttonTest);
    }
}
