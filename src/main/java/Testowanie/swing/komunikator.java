package Testowanie.swing;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class komunikator extends JFrame implements Runnable {

    //Logic
    ConnectionObject<String> connection;

    //Interface
    JMenuBarGui menuBar;

    JPanel messageBar;
    JTextField messegeJTextField;
    JButton messegeJButton;

    JScrollPane messageHistoryjScrollPane;
    JEditorPane jEditorPaneMessageHistory;
    public komunikator() throws HeadlessException {
        super();
    }


    @Override
    public void run() {
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(900,900);
        setLayout(new GridBagLayout());
        setLocationRelativeTo(null);
        menuBar = new JMenuBarGui();
        setJMenuBar(menuBar);

        gridBagConstraints.gridx=0;
        gridBagConstraints.gridy=0;
        jEditorPaneMessageHistory = new JEditorPane();
        messageHistoryjScrollPane = new JScrollPane(jEditorPaneMessageHistory);
        jEditorPaneMessageHistory.setFocusable(false);

        jEditorPaneMessageHistory.setPreferredSize(new Dimension(500,500));
        getContentPane().add(messageHistoryjScrollPane,gridBagConstraints);


        gridBagConstraints.gridx=0;
        gridBagConstraints.gridy=1;
        messageBar = new JPanel();
        messageBar.setLayout(new BoxLayout(messageBar,BoxLayout.X_AXIS));
        messegeJTextField = new JTextField();
        messegeJTextField.setPreferredSize(new Dimension(500,20));



        messageBar.add(messegeJTextField);
        messegeJButton = new JButton("Wyslij");
        messageBar.add(messegeJButton);
        getContentPane().add(messageBar,gridBagConstraints);
        this.setVisible(true);
    }

    class JMenuBarGui extends JMenuBar{
        JMenu jMenuConnection;
        JMenuItem connectionCreate;
        JMenu other;
        JMenuItem otherOther;
        public JMenuBarGui() {
            jMenuConnection = new JMenu("Connection");
            connectionCreate = new JMenuItem("Connect to");
            other = new JMenu("Other");
            otherOther = new JMenuItem("Other option");
            jMenuConnection.add(connectionCreate);
            add(jMenuConnection);
            other.add(otherOther);
            add(other);

            connectionCreate.addActionListener(e -> {
                JTextField jTextFieldIP = new JTextField();
                JTextField jTextFieldPort = new JTextField();
                JTextField jTectFieldNick = new JTextField();
                Object [] fields = {
                        "Ip",jTextFieldIP,
                        "Port",jTextFieldPort,
                        "nick",jTectFieldNick
                };

                jTextFieldIP.setText("localhost");
                jTextFieldPort.setText("6666");
                jTectFieldNick.setText("Osoba");

                int dialog = JOptionPane.showConfirmDialog(connectionCreate, fields, "Create connection", JOptionPane.OK_CANCEL_OPTION);
                if(dialog == JOptionPane.OK_OPTION){

                    String ipString = jTextFieldIP.getText();
                    String portString = jTextFieldPort.getText();
                    String nickString = jTectFieldNick.getText();
                        new Thread(() -> {
                            try {

                                connection = new ConnectionObject<String>(ipString,Integer.parseInt(portString));
                                JOptionPane.showMessageDialog(this,"klient sie podlaczyl","polaczenie",JOptionPane.OK_OPTION);
                                messegeJButton.addActionListener(new sendButtonListener());
                                new Thread(new receivedMessageThread()).start();

                            } catch (IOException ioException) {
                                ioException.printStackTrace();
                            }
                        }).start();
                    JOptionPane.showMessageDialog(this,"Oczekiwanie na klienta","Oczekiwanie",JOptionPane.OK_OPTION);

                }

            });
        }
    }

    class sendButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String messegeText = messegeJTextField.getText();
            messegeJTextField.setText("");

            System.out.println("Wysylanie : "+messegeText);
            try {
                connection.writeObject(messegeText);
                Document document = jEditorPaneMessageHistory.getDocument();
                document.insertString(document.getLength(),messegeText+"\n",null);
            } catch (IOException | BadLocationException ioException) {
                ioException.printStackTrace();
            }

        }
    }

    class receivedMessageThread implements Runnable{
        @Override
        public void run() {
            while(true){

                try {
                    String recivedMessage = connection.readObject()+"\n";
                    System.out.println("Odebrano: "+recivedMessage);
                    Document document = jEditorPaneMessageHistory.getDocument();
                    document.insertString(document.getLength(),recivedMessage,null);

                } catch (IOException e) {
                    break;
                } catch (ClassNotFoundException | BadLocationException e) {
                    e.printStackTrace();
                }

            }
            JOptionPane.showMessageDialog(messegeJButton,"Problem z polaczeniem","",JOptionPane.OK_OPTION);
        }
    }

}


class runner2{
    public static void main(String[] args) {
        komunikator komunikator = new komunikator();
        SwingUtilities.invokeLater(komunikator);
    }
}
