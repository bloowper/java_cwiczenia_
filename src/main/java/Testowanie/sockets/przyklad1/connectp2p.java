package Testowanie.sockets.przyklad1;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class connectp2p{

    Socket socket;
    connectp2p(String ip, int port) throws IOException {
        try {
            socket = new Socket(ip,port);
        } catch (IOException e) {

            try {
                ServerSocket serverSocket = new ServerSocket(port);
                socket = serverSocket.accept();

                OutputStream outputStream = socket.getOutputStream();
            } catch (IOException ioException) {
                throw ioException;
            }
        }
    }


    DataInputStream dataInputStream() throws IOException {
        return new DataInputStream(socket.getInputStream());
    }


    DataOutputStream outputStream() throws IOException {
        return new DataOutputStream(socket.getOutputStream());
    }

    void close() throws IOException {
        if(socket!=null)
        if(!socket.isClosed()){
            socket.close();
        }

    }
}


class runner2{
    public static void main(String[] args) throws IOException {
        connectp2p localhost = new connectp2p("localhost", 44444);
        DataInputStream dataInputStream = localhost.dataInputStream();
        DataOutputStream dataOutputStream = localhost.outputStream();

        new Thread(() -> {
            byte[] temp = new byte[125];

            while (true) {
                try {
                    try {
                        int available = dataInputStream.available();
                        if(available>0){
                            dataInputStream.read(temp, 0, available);
                            String dane = new String(temp, 0, available, StandardCharsets.UTF_8);
                            System.out.println(dane);
                        }

                        Thread.sleep(500);
                    } catch (IOException | InterruptedException e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }).start();


        BufferedReader keyboardReader = new BufferedReader(new InputStreamReader(System.in));
        String keyboardLine;
        while ((keyboardLine = keyboardReader.readLine())!=null){
            dataOutputStream.writeBytes(keyboardLine);
            dataOutputStream.flush();
        }

    }


}



