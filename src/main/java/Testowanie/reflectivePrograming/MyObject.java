package Testowanie.reflectivePrograming;

public class MyObject {
    public int myint1;
    public int myint2;
    public String mystring;

    public MyObject(int myint1, int myint2, String mystring) {
        this.myint1 = myint1;
        this.myint2 = myint2;
        this.mystring = mystring;
    }

    public int getMyint1() {
        return myint1;
    }

    public void setMyint1(int myint1) {
        this.myint1 = myint1;
    }

    public int getMyint2() {
        return myint2;
    }

    public void setMyint2(int myint2) {
        this.myint2 = myint2;
    }

    public String getMystring() {
        return mystring;
    }

    public void setMystring(String mystring) {
        this.mystring = mystring;
    }

    public int kwadrat(int value){
        return value*value;
    }

    @Override
    public String toString() {
        return "myobject{" +
                "myint1=" + myint1 +
                ", myint2=" + myint2 +
                ", mystring='" + mystring + '\'' +
                '}';
    }
}
