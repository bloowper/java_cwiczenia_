package Testowanie.reflectivePrograming;


import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class test1 {
    public static void main(String[] args) {


        try {
            Class<?> aClass = Class.forName("Testowanie.reflectivePrograming.MyObject");

//            Constructor<?>[] constructor = aClass.getConstructors();
//            for (Constructor<?> constructor1 : constructor) {
//                System.out.println(constructor1);
//            }

            //ok mamy odstepne takie konstruktory ale ja chce jeden konkretny


            //bierzemy sobie odpowiedni konstruktor
            Constructor<?> constructor1 = aClass.getConstructor(int.class, int.class, String.class);

            //tworzymy sobie obiekt
            Object myObject = constructor1.newInstance(1, 1, "Dobry dzien");
            System.out.println(myObject);

            //pobieram metote poprzez nazwe i parametry i potem ja wywoluje
            Method method = aClass.getMethod("kwadrat", int.class);
            Object invoke = method.invoke(myObject, 5);
            System.out.println(invoke);


            Field[] fields = aClass.getFields();
            for (Field field : fields) {
                System.out.println(field);
            }

            Field mystring = aClass.getField("mystring");
            mystring.set(myObject,"NOWA WARTOSC");
            System.out.println(myObject);


            Object nowy_obiekt = Class.forName("Testowanie.reflectivePrograming.MyObject").getConstructor(int.class, int.class, String.class).newInstance(10, 10, "nowy obiekt");
            System.out.println(nowy_obiekt);
            Class<?> aClass1 = Class.forName("Testowanie.reflectivePrograming.MyObject");
            Field myint1 = aClass1.getField("myint1");
            Field myint2 = aClass1.getField("myint2");
            myint1.set(nowy_obiekt,10000);
            myint2.set(nowy_obiekt,10000);
            System.out.println(nowy_obiekt);



        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException | NoSuchFieldException e) {
            e.printStackTrace();
        }

    }
}
