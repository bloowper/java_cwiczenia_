package Testowanie.javaXML;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.Vector;

/**
 * Klasa do prezentacji tworzenia obiektow na podstawie dokumentu XML. na podstawie parsera DOOM
 */
public class doomXmlToObject  {

    public static void main(String[] args) throws Exception {
        //przygotuje juz sobie wektor na te kontenerOsob
        Vector<Osoba> kontenerOsob = new Vector<>();

        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document document = builder.parse(new File("src/main/java/Testowanie/javaXML/klasa.xml"));

        //to nas tylko poinformuje o nazwie noda najwyzej w hierarchi
        System.out.println("Otworzono : "+document.getDocumentElement().getNodeName());

        NodeList byTagName = document.getElementsByTagName("osoba");
        for(var i=0; i<byTagName.getLength();i++){
            Node node = byTagName.item(i);

            //jezeli to jest node elementow to odczytujemy z niej dane
            if(node.getNodeType() == Node.ELEMENT_NODE){

                Element element = (Element) node;
                String numer = element.getElementsByTagName("numer").item(0).getTextContent();
                String imie = element.getElementsByTagName("imie").item(0).getTextContent();
                String nazwisko = element.getElementsByTagName("nazwisko").item(0).getTextContent();
                kontenerOsob.add(new Osoba(Integer.parseInt(numer),imie,nazwisko));

            }
        }

        //przejrzyjmy kontener osob
        kontenerOsob.forEach(System.out::println);


//Dodawanie
        //wymiekam przy tym





//ZAPISYWANIE
        //tworzymy transformator ktory zapisze nam pliki z tego XML w pamieci do pliku
        Transformer transformer = TransformerFactory.newInstance().newTransformer();

        //tworzymy wejscie
        DOMSource source = new DOMSource(document);

        //tworzymy wyjscie
        StreamResult result = new StreamResult(new File("src/main/java/Testowanie/javaXML/klasaCOPPY.xml"));

        //zapisujemy przy uzyciu transformatora

        transformer.transform(source,result);


        //zapiszmy sobie teraz ten dokument XML




    }
}

class Osoba {
    int numer;
    String imie;
    String nazwisko;

    public Osoba(int numer, String imie, String nazwisko) {
        this.numer = numer;
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

    @Override
    public String toString() {
        return "osoba{" +
                "numer=" + numer +
                ", imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                '}';
    }
}