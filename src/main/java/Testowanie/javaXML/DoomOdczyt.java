package Testowanie.javaXML;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;

//https://mkyong.com/java/how-to-read-xml-file-in-java-dom-parser/
public class DoomOdczyt {
    public static void main(String[] args) throws Exception {
        DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document document = documentBuilder.parse(new File("src/main/java/Testowanie/javaXML/stuff.xml"));
        document.getDocumentElement().normalize();//OPCJONALNE ALE ZALECANE



        //sam top dokumentu xml
        System.out.println(document.getDocumentElement().getNodeName());

        //teraz chcemy dostac sie do jego dzieci i przeloopowac przez nei
        //jego dzieci to sa nody typu staff
        NodeList staffNode = document.getElementsByTagName("staff");
        for(var i=0;i<staffNode.getLength();i++){
            Node node = staffNode.item(i);
            System.out.println("Current elemet\t"+node.getNodeName());
            //w tym momencie ten node moze zawierac juz elementy albo jeszcze miec dzieci + elementy
            //node z elementami to ElementNode chyba
            if(node.getNodeType() == Node.ELEMENT_NODE){
                Element element = (Element) node;
                System.out.println("\t"+element.getElementsByTagName("firstname").item(0).getTextContent());
                System.out.println("\t"+element.getElementsByTagName("lastname").item(0).getTextContent());
                System.out.println("\t"+element.getElementsByTagName("nickname").item(0).getTextContent());

            }
        }

    }
}
