package zadanie8;

//Swing doprowadzil mnie do czerwonej goraczki
//po prostdu chce skonczyc to zadanie ;-; juz mniejsza
//o zgrabnosc rozwiazania ;--------;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Vector;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


//TODO 1
// poprawic jakos alligment personview zeby byly wyrownanie do prawej

//TODO 2
// dodac sprawdzanie wprowadzanych danych tzn zeby numer byl numerem ect




public class GUI extends JFrame implements Runnable {

    GridBagConstraints constraints;
    JMenuBarGUI menuBar;
    File fileData;

    PersonsLoader personsLoader;
    JPanel personViewList; // Pojemnik dla view osob
    Vector<PersonController> personControllers;

    public GUI() throws HeadlessException {
        super();
        constraints = new GridBagConstraints();
    }

    @Override
    public void run() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(900,900);
        setLayout(new GridBagLayout());

        personControllers = new Vector<>();
        personViewList = new JPanel();
        BoxLayout layoutPersonViewList = new BoxLayout(personViewList, BoxLayout.Y_AXIS);
        personViewList.setLayout(layoutPersonViewList);

        constraints.gridx=1;constraints.gridy=1;
        getContentPane().add(personViewList,constraints);

        menuBar = new JMenuBarGUI();
        this.setJMenuBar(menuBar);




        //pack();
        setVisible(true);
    }


    //TODO ogarnac odrobine kolejnosc inicjalizacji obiektow
    // w zalozeniu chcialem jew inicjalizowac w miejscu odpowiednim do polozenia w JmenuBarze
    // ale chyba to nie zdaje swojego zadania, tzn mozliwe bedzie uzycie zasobow nie zainicjalizowanych
    class JMenuBarGUI extends  JMenuBar{

        JMenu file;
            JMenuItem fileLoad;
            JMenuItem fileSave;
            JMenuItem fileCreateNewDataFile;
            final JFileChooser jFileChooser = new JFileChooser("src/main/java/zadanie8");
        JMenu edit;
            JMenuItem editDelete;
            JMenuItem editAdd;

        public JMenuBarGUI() {
            super();
            file = new JMenu("FIle");
            add(file);
            fileLoad = new JMenuItem("Load from file");
            fileSave = new JMenuItem("Save");
            fileCreateNewDataFile = new JMenuItem("Create new data file");
            file.add(fileLoad);
            file.add(fileSave);
            file.add(fileCreateNewDataFile);
    //FILE
    //FILE/LOAD FILE
            FileNameExtensionFilter filter = new FileNameExtensionFilter("XML", "xml","XML");
            jFileChooser.setFileFilter(filter);
            fileLoad.addActionListener(e -> {

                jFileChooser.setFileSelectionMode(JFileChooser.OPEN_DIALOG);
                int dialog = jFileChooser.showOpenDialog(this);
                if(dialog == JFileChooser.APPROVE_OPTION){
                    File selectedFile = jFileChooser.getSelectedFile();
                    fileData = selectedFile;
                    System.out.println(fileData);
                    personsLoader = new PersonsLoader(fileData);
                    try {
                        Vector<PersonModel> models = personsLoader.loadFromFile();
                        models.stream().forEach(model -> {
                            PersonView view = new PersonView();
                            personControllers.add(new PersonController(model,view));
                            personViewList.add(view);
                        });
                        SwingUtilities.updateComponentTreeUI(personViewList);

                    } catch (FileNotFoundException fileNotFoundException) {
                        JOptionPane.showMessageDialog(menuBar,"Problem with file");
                    }
                }

            });
    //FILE/SAVE FILE
            fileSave.addActionListener(e -> {
                if(personsLoader == null){
                    JOptionPane.showMessageDialog(menuBar,"File not load yet");
                    //TODO dodanie opcji wygenerowania nowego pliku .xml dla danych
                }else {
                    var personModels = personControllers.stream().map(PersonController::getPersonModel).collect(Collectors.toCollection(Vector::new));
                    try {
                        personsLoader.saveToFile(personModels);
                    } catch (FileNotFoundException fileNotFoundException) {
                        JOptionPane.showMessageDialog(this,"Error occurred while saving to file","Error",JOptionPane.ERROR_MESSAGE);
                    }
                }

            });


    //FILE/Create new data file
            fileCreateNewDataFile.addActionListener(e -> {
                int result = jFileChooser.showSaveDialog(fileCreateNewDataFile);
                if (result == JFileChooser.APPROVE_OPTION) {

                    if(!jFileChooser.getSelectedFile().toString().endsWith(".xml")){
                        JOptionPane.showMessageDialog(jFileChooser,"File have to end with .xml","Error",JOptionPane.ERROR_MESSAGE);
                    }else {

                        if (! jFileChooser.getSelectedFile().exists()) {
                            try {
                                jFileChooser.getSelectedFile().createNewFile();
                                File file = jFileChooser.getSelectedFile();
                                System.out.println("New data file: "+file);
                                personsLoader = new PersonsLoader(jFileChooser.getSelectedFile());
                                fileData = file;
                            } catch (IOException ioException) {
                                JOptionPane.showMessageDialog(this,"Error occurred while saving to file","Error",JOptionPane.ERROR_MESSAGE);

                            }
                        } else {
                            //taki plik juz istnieje a my chicelismy stworzyc nowy
                            JOptionPane.showMessageDialog(this,"File currently exist","Error",JOptionPane.ERROR_MESSAGE);

                        }

                    }
                }

            });

    //EDIT
            edit = new JMenu("Edit");
            add(edit);
            editDelete = new JMenuItem("Delete selected");
            edit.add(editDelete);
    //EDIT/DELETE SELECTED
            editDelete.addActionListener(e -> {

                int dialog = JOptionPane.showConfirmDialog(this, "Are you sure", "Delet", JOptionPane.YES_NO_OPTION);
                if(dialog == JOptionPane.OK_OPTION){
                    System.out.println("Deleting elements pressed");
                    var collect = personControllers.stream().filter(personController -> personController.getPersonView().isSelected()).collect(Collectors.toCollection(Vector::new));
                    collect.stream().forEach(personController -> System.out.println("Delete : "+personController.getPersonModel()));
                    for (PersonController controller : collect) {
                        personViewList.remove(controller.getPersonView());
                        SwingUtilities.updateComponentTreeUI(personViewList);
                    }
                    personControllers.removeAll(collect);

                }


            });
    //EDIT/ADD NEW
            editAdd = new JMenuItem("Add new");
            edit.add(editAdd);
            editAdd.addActionListener(e -> {

                if (fileData != null) {
                    JTextField firstName = new JTextField();
                    JTextField secondName = new JTextField();
                    JTextField phoneNumbr = new JTextField();
                    Object [] fields = {
                            "Name",firstName,
                            "Last name",secondName,
                            "Phone number",phoneNumbr
                    };


                    int dialog = JOptionPane.showConfirmDialog(this, fields, "Enter data", JOptionPane.OK_CANCEL_OPTION);
                    if(dialog == JOptionPane.OK_OPTION){
                        String firstNameText = firstName.getText();
                        String secondNameText = secondName.getText();
                        String phoneNumbrText = phoneNumbr.getText();
                        //TODO sprawdzic dane wejsciowe


                        String numberPattern = "[\\d, ]{6,15}";
                        Pattern pattern = Pattern.compile(numberPattern);

                        if( ! pattern.matcher(phoneNumbrText).matches()){
                            JOptionPane.showMessageDialog(editAdd,"the number format is incorret","Validation problem",JOptionPane.ERROR_MESSAGE);

                        }
                        else {
                            PersonController controller = new PersonController(new PersonModel(firstNameText, secondNameText, phoneNumbrText), new PersonView());
                            System.out.println("added : "+controller.getPersonModel());//informacje do konsoli
                            personViewList.add(controller.getPersonView());
                            personControllers.add(controller);
                            SwingUtilities.updateComponentTreeUI(this);
                        }

                    }
                } else {
                    JOptionPane.showMessageDialog(menuBar,"Load file \nor create new data file in\nFile\\create new data file");

                }

            });

        }
    }

}
