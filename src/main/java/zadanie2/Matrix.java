package zadanie2;

//sprobuje wykorzystac troche test drive development
//bo nigdy jeszcze nie mialem okazji

import java.util.Arrays;

public class Matrix {
    private final double[][] data;



    public Matrix(double[][] data){
        this.data = data;
    }



    public Matrix(int size){
        this.data = new double[size][size];
    }



    public void set_cell(int row , int column,double value){
        if(row>=data.length || column>=data[0].length){
            throw new IllegalArgumentException("row or column out of bound");
        }
        this.data[row][column] = value;
    }



    public double get_cell(int row,int column){
        if(row>=data.length || column>=data[0].length){
            throw new IllegalArgumentException("row or column out of bound");
        }
        return data[row][column];
    }



    public int getSize(){
        return data.length;
    }



    public Matrix add(Matrix right){
        if(this.getSize() != right.getSize())
            throw new IllegalArgumentException();
        Matrix matrixToReturn = new Matrix(this.data.length);
        for(int w=0;w<this.getSize();w++)
        {
            for(int k=0;k<this.getSize();k++)
            {
                matrixToReturn.set_cell(w,k,
                        this.get_cell(w,k) + right.get_cell(w,k));
            }
        }
        return matrixToReturn;
    }



    public Matrix multiply(Matrix right){
        if(this.data[0].length != right.data.length){
            throw new IllegalArgumentException();
        }
        Matrix matrixToReturn = new Matrix(this.data.length);
        for(int w=0;w<this.data.length;w++){
            for(int k=0;k<this.data.length;k++){
                double tempValue = 0;
                for(var x=0;x<this.data.length;x++){
                    tempValue+= data[w][x] * right.data[x][k];
                }
                matrixToReturn.data[w][k] = tempValue;
            }
        }
        return matrixToReturn;
    }



    public Matrix multiply(double right){
        Matrix matrix = new Matrix(this.data.length);
        for(int w=0;w<matrix.data.length;w++) {
            for(int k=0;k<matrix.data.length;k++){
                matrix.data[w][k] = this.data[w][k]*right;
            }
        }
        return matrix;
    }



    @Override
    public String toString() {
        return "Matrix{" +
                "data=" + Arrays.deepToString(data) +
                '}';
    }



    public double det(){
        if(data.length==1){
            return data[0][0];
        }
        else if(data.length==2){
            /*
            00 01
            10 11
             */
            return data[0][0] * data[1][1] - data[1][0] * data[0][1];
        }
        else if(data.length==3){
            /*
            00 01 02
            10 11 12
            20 21 22
             */
            return data[0][0]*data[1][1]*data[2][2] + data[0][1]*data[1][2]*data[2][0] + data[0][2]*data[1][0]*data[2][1] -(data[2][0]*data[1][1]*data[0][2] + data[2][1]*data[1][2]*data[0][0] + data[2][2]*data[1][0]*data[0][1]);
        }
        double toReturn=0;
        for(int i=0;i<data.length;i++){
             toReturn += this.minor(0,i).det() * data[0][i] * Math.pow(-1,i+2) ;
                        //+1 bo idziemy po pierwszym wierszu +1 zeby skompensowac liczenie od 0 nie od 1
        }
        return toReturn;
    }



     Matrix minor(int rSkip, int cSkip){
        if(this.data.length<2)
            throw new IllegalCallerException();
        Matrix matrix = new Matrix(this.data.length - 1);
        for(int rPer=0;rPer<data.length;rPer++){
            for(int cPer=0;cPer<data.length;cPer++){
                if(rPer==rSkip || cPer==cSkip) continue;
                 int r=(rPer>=rSkip)?rPer-1:rPer;
                 int c=(cPer>=cSkip)?cPer-1:cPer;
                 matrix.data[r][c] = data[rPer][cPer];
            }
        }
        return matrix;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Matrix matrix = (Matrix) o;
        return Arrays.deepEquals(data, matrix.data);
    }

}
