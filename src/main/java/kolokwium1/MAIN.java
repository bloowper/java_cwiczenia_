package kolokwium1;

import java.io.*;
import java.util.HashMap;

public class MAIN {
    public static void main(String[] args) {
        String fileInput;
        String fileOutput;
        if(args.length==0) {
            fileInput = "input.txt";
            fileOutput = "output.txt";
        }
        else{
            fileInput = args[0];
            fileOutput = args[1];
        }

        //try with resources
        StringBuilder stringBuilder = new StringBuilder();
        try(var br = new BufferedReader(new FileReader(new File(fileInput)))){
            String temp;
            while((temp=br.readLine())!=null) stringBuilder.append(temp);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String tekst = stringBuilder.toString();

        //Tworzenie histogramu tekstu
        StringBuilder stringBuilderHistogram = new StringBuilder();
        HashMap<String, Integer> stringIntegerHashMap = MyUtilities.wordHistogram(tekst);
        stringIntegerHashMap.forEach((s, integer) -> {
            stringBuilderHistogram.append(s + " - "+integer+"\n");
        });
        String sHistogram = stringBuilderHistogram.toString();

        try( var bw = new BufferedWriter(new FileWriter(new File(fileOutput)))){
            bw.write(sHistogram);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //zakladam ze nie ma ., ect
        String[] slowa = tekst.split(" ");



    }
}
