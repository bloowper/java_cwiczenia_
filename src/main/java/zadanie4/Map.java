package zadanie4;

import java.util.ArrayList;
import java.util.Iterator;

public class Map<K,V> implements Iterable<Node<K, V>> {
    //wybralem chyba najwolniejsza mozliwa implementację:na liscie
    ArrayList<Node<K,V>> map=null;

    public Map(){
        this.map = new ArrayList<>();
    }

    public V put(K key, V value){
        boolean added=false;
        for(int i=0;i<map.size();i++) {
            if (map.get(i).getKey().equals(key)) {
                map.get(i).setValue(value);
                added = true;
            }
        }
        if(!added){
            map.add(new Node<>(key,value));
        }
        return value;
    }

    public boolean containsKey(K key){
        for(Node<K,V> elem:map){
            if(elem.getKey().equals(key)) return true;
        }
        return false;
    }

    public V get(K key){
        for(int i=0;i<map.size();i++){
            if(map.get(i).getKey().equals(key)) return map.get(i).getValue();
        }
        return null;
    }

    int size(){
        return map.size();
    }

    void acceptVisitor(Visitor<K> visitor){
        map.removeIf(kvNode -> {
            return visitor.shouldRemove(kvNode.getKey());
        });
    }

    @Override
    public Iterator<Node<K, V>> iterator() {
        return this.map.iterator();
    }
}
