package zadanie4;

public interface Visitor<K>{
    boolean shouldRemove(K obj);
}
