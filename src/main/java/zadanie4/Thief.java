package zadanie4;

public class Thief implements Visitor<Integer> {
    Integer minValue=null;

    public Thief(Integer minValue) {
        this.minValue = minValue;
    }

    @Override
    public boolean shouldRemove(Integer obj) {
        return obj>minValue;
    }
}
