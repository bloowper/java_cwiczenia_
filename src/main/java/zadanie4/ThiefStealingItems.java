package zadanie4;



public class ThiefStealingItems{
    public static void main(String[] args) {
        Map<Integer, String> przedmioty = new Map<>();
        przedmioty.put(300,"Stol");
        przedmioty.put(400,"zestaw krzesel");
        przedmioty.put(5000,"laptop");
        przedmioty.put(2500,"konsola");
        przedmioty.put(200,"zestaw talerzy");
        przedmioty.put(50,"Stara ladowarka");
        przedmioty.put(1500,"bizuteria");

        for(Node<Integer,String> elem:przedmioty){
            System.out.println(elem.getKey()+" -> "+elem.getValue());
        }

        System.out.println("\nZLODZIEJ KRADNIE!\n");
        Thief thief = new Thief(1000);
        przedmioty.acceptVisitor(thief);

        System.out.println("zostalo\n");
        for(Node<Integer,String> elem:przedmioty){
            System.out.println(elem.getKey()+" -> "+elem.getValue());
        }
    }
}
