# Zadania cwiczenia java #
* @author Tomasz Orchowski
* semestr 2020 /2021    
## zadanie 2 ##

## zadanie 3 ##

## zadanie 4 ##
Proszę napisać klasę reprezentującą mapę typu klucz->wartość, gdzie i klucz, i wartość mogą być dowolnego typu. Mapa powinna mieć podstawowe metody umożliwiające:
dodawanie elementów (dodanie elementu o użytym już wcześniej kluczu podmienia ten stary)
dostęp do wartości przez klucz
sprawdzanie, czy isnieje element o podanym kluczu
Ponadto, mapa powinna mieć metodę acceptVisitor, która przyjmuje obiekt o specjalnym interfejsie Visitor i usunąć z siebie wszystkie elementy, które Visitor każe usunąć. Interfejs Visitor ma mieć metodę shouldRemove przyjmującą pojedynczy klucz z mapy i ta metoda na podstawie własnych, zależnych od implementacji kryteriów, ma zwracać true, jeśli element powinien być usunięty, a false, jeśli nie.

Następnie proszę utworzyć klasę dla (klucz: int) i (wartość: String) reprezentująca nazwy urządzeń domowych indeksowane ich cenami i "wizytę" klasy Thief (implementujacej Visitor). Po wizycie złodzieja gospodarze powinni zostać pozbawieni przedmiotów o wartości większej niż 1000 zł.
Należy użyć typów generycznych w rozwiązaniu. Nie można używać instanceof i getClass() (wyjątkiem może być metoda Object::equals, jeśli będzie potrzebna). Dozwolone (a nawet zalecane) jest korzystanie z kolekcji i algorytmów JDK.
## zadanie 5 ##
Proszę załadować plik tekstowy podany jako argument linii poleceń, a następnie wyświetlić dla niego statystyki:
1) liczba niebiałych znaków
2) liczba wyrazów
3) liczba zdań (proszę opisać jaką definicję zdania Państwo przyjęli)

Ponadto, program powinien wyświetlić 10 najczęściej występujących wyrazów wraz z ilością wystąpień. Jeśli 10 miejsce jest zajęte przez więcej niż jeden wyraz, należy wyświetlić wszystkie wyrazy zajmujący ex aequo to miejsce.
## zadanie 6 ##
Proszę napisać grę w kółko i krzyżyk z interfejsem graficznym dla 2 osób.
Gra powinna obsługiwać dowolne wymiary planszy oraz dowolną ilość
 jednakowych symboli w pionie, poziomie lub po ukosie jako warunek 
 wygranej. Aplikacja powinna poprosić użytkownika o wprowadzenie tych
  rozmiarów przed rozpoczęciem, również w okienku graficznym, i na 
  podstawie wprowadzonych wartości przygotować planszę do rozgrywki.

## zadanie 7
Termin: 18 grudnia 2020 23:19
Instrukcje
Proszę przerobić ostatnie zadanie (kółko i krzyżyk) na wersję "online" - żeby gracze grali ze sobą przez sieć. Architektura rozwiązania może być dowolna - serwerem może być osobny program, z którym obaj gracze się łączą, może być również wbudowany w program z grą. Sposób przesyłania danych również jest dowolny. Proszę natomiast zachować funkcjonalność wersji "offline" - możliwość wyboru rozmiaru planszy i warunku wygranej.

Jako że w rozwiązaniach poprzedniego zadania zauważyłem powtarzające się problemy, chciałbym uściślić pewne kwestie:
Warunek wygranej może być mniejszy niż rozmiar planszy
Jednakowe symbole muszą się obok siebie, żeby nastąpiła wygrana
Nie można nadpisywać ruchu przeciwnika
Należy sprawdzać wszystkie "ukosy" - nie tylko główne przekątne
Należy pamiętać, że wszystkie zmiany GUI muszą się dokonywać na wątku EDT (SwingUtilities.invokeLater, SwingUtilities.invokeAndWait). Jednocześnie nie wolno blokować tego wątku np. oczekiwaniem na ruch przeciwnika - musi się to odbywać w innym wątku.


## Zadanie 8 ##

Proszę napisać elektroniczną książkę telefoniczną z interfejsem graficznym. Książka powinna przechowywać trójki (imię, nazwisko, telefon) oraz mieć możliwość dodawania, usuwania i edycji osób. Powinna również umożliwiać zapisywanie i odczytywanie osób do/z pliku XML.
Format pliku XML jest dowolny, w szczególności można użyć XMLEncodera(Decodera). Zachęcam do skorzystania z tej opcji - zapoznają się Państwo z JavaBeans i zmusi to Państwa do oddzielenia danych od interfejsu. Może się przydać wzorzec projektowy Model-Widok-Kontroler (odsyłam do internetu).


## Zadanie 9
Instrukcje
Proszę napisać książkę telefoniczną z rekordami (imię, nazwisko, telefon) przechowującą dane w bazie danych SQL. Książka powinna umożliwiać dodawanie, usuwanie i edycję wpisów.

Można przerobić/rozszerzyć ostatnie zadanie GUI, aby korzystało z SQL, albo napisać nową, tekstową aplikację implementującą powyższą funkcjonalność.


## Zadanie 10
### Termin: 26 stycznia 2021 23:59
* Instrukcje
Proszę napisać klasę, która wypisuje informacje o klasie na podstawie jej nazwy podanej jako argument wywołania programu. W informacjach powinny się znaleźć:
nazwa klasy
modyfikatory klasy
wszystkie pola klasy (publiczne, prywatne, ...) bez pól dziedziczonych wraz z ich typami i modyfikatorami
wszystkie metody klasy (podobnie jak dla pól, bez metod dziedziczonych) ze zwracaną wartością, modyfikatorami, typami argumentów i listą zrzucanych wyjatków
Jako nazw typów należy używać "krótkich" nazw, bez pakietów (getSimpleName()). W załączniku jest przykładowe wyjście programu dla klasy Class.

Do rozkodowania wartości zwracanej przez getModifiers() może się przydać klasa Modifier (odsyłam do dokumentacji).
